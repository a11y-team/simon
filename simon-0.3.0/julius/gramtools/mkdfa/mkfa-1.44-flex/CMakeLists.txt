#project(mkfa)
FIND_PACKAGE(FLEX)
FIND_PACKAGE(BISON)

#add_definitions(-lang-c-c++-comments -std=c99 )

FLEX_TARGET(Lex ${CMAKE_CURRENT_SOURCE_DIR}/gram.l  ${CMAKE_CURRENT_SOURCE_DIR}/gram.lex.c)
BISON_TARGET(Gram ${CMAKE_CURRENT_SOURCE_DIR}/gram.y ${CMAKE_CURRENT_SOURCE_DIR}/gram.tab.c)

#ADD_FLEX_BISON_DEPENDENCY(Lex Gram)

set (mkfa_SRCS
${CMAKE_CURRENT_SOURCE_DIR}/dfa.c
${CMAKE_CURRENT_SOURCE_DIR}/main.c
${CMAKE_CURRENT_SOURCE_DIR}/nfa.c
${CMAKE_CURRENT_SOURCE_DIR}/triplet.c
${CMAKE_CURRENT_SOURCE_DIR}/voca.c)

add_executable(mkfa ${mkfa_SRCS} ${BISON_Gram_OUTPUTS})
target_link_libraries(mkfa ${FLEX_LIBRARIES})
install (TARGETS mkfa DESTINATION bin COMPONENT julius)
