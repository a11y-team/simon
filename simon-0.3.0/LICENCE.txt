================   simon Licence   ==================


		    GNU GENERAL PUBLIC LICENSE
		       Version 2, June 1991

 Copyright (C) 1989, 1991 Free Software Foundation, Inc.,
 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA
 Everyone is permitted to copy and distribute verbatim copies
 of this license document, but changing it is not allowed.

			    Preamble

  The licenses for most software are designed to take away your
freedom to share and change it.  By contrast, the GNU General Public
License is intended to guarantee your freedom to share and change free
software--to make sure the software is free for all its users.  This
General Public License applies to most of the Free Software
Foundation's software and to any other program whose authors commit to
using it.  (Some other Free Software Foundation software is covered by
the GNU Lesser General Public License instead.)  You can apply it to
your programs, too.

  When we speak of free software, we are referring to freedom, not
price.  Our General Public Licenses are designed to make sure that you
have the freedom to distribute copies of free software (and charge for
this service if you wish), that you receive source code or can get it
if you want it, that you can change the software or use pieces of it
in new free programs; and that you know you can do these things.

  To protect your rights, we need to make restrictions that forbid
anyone to deny you these rights or to ask you to surrender the rights.
These restrictions translate to certain responsibilities for you if you
distribute copies of the software, or if you modify it.

  For example, if you distribute copies of such a program, whether
gratis or for a fee, you must give the recipients all the rights that
you have.  You must make sure that they, too, receive or can get the
source code.  And you must show them these terms so they know their
rights.

  We protect your rights with two steps: (1) copyright the software, and
(2) offer you this license which gives you legal permission to copy,
distribute and/or modify the software.

  Also, for each author's protection and ours, we want to make certain
that everyone understands that there is no warranty for this free
software.  If the software is modified by someone else and passed on, we
want its recipients to know that what they have is not the original, so
that any problems introduced by others will not reflect on the original
authors' reputations.

  Finally, any free program is threatened constantly by software
patents.  We wish to avoid the danger that redistributors of a free
program will individually obtain patent licenses, in effect making the
program proprietary.  To prevent this, we have made it clear that any
patent must be licensed for everyone's free use or not licensed at all.

  The precise terms and conditions for copying, distribution and
modification follow.

		    GNU GENERAL PUBLIC LICENSE
   TERMS AND CONDITIONS FOR COPYING, DISTRIBUTION AND MODIFICATION

  0. This License applies to any program or other work which contains
a notice placed by the copyright holder saying it may be distributed
under the terms of this General Public License.  The "Program", below,
refers to any such program or work, and a "work based on the Program"
means either the Program or any derivative work under copyright law:
that is to say, a work containing the Program or a portion of it,
either verbatim or with modifications and/or translated into another
language.  (Hereinafter, translation is included without limitation in
the term "modification".)  Each licensee is addressed as "you".

Activities other than copying, distribution and modification are not
covered by this License; they are outside its scope.  The act of
running the Program is not restricted, and the output from the Program
is covered only if its contents constitute a work based on the
Program (independent of having been made by running the Program).
Whether that is true depends on what the Program does.

  1. You may copy and distribute verbatim copies of the Program's
source code as you receive it, in any medium, provided that you
conspicuously and appropriately publish on each copy an appropriate
copyright notice and disclaimer of warranty; keep intact all the
notices that refer to this License and to the absence of any warranty;
and give any other recipients of the Program a copy of this License
along with the Program.

You may charge a fee for the physical act of transferring a copy, and
you may at your option offer warranty protection in exchange for a fee.

  2. You may modify your copy or copies of the Program or any portion
of it, thus forming a work based on the Program, and copy and
distribute such modifications or work under the terms of Section 1
above, provided that you also meet all of these conditions:

    a) You must cause the modified files to carry prominent notices
    stating that you changed the files and the date of any change.

    b) You must cause any work that you distribute or publish, that in
    whole or in part contains or is derived from the Program or any
    part thereof, to be licensed as a whole at no charge to all third
    parties under the terms of this License.

    c) If the modified program normally reads commands interactively
    when run, you must cause it, when started running for such
    interactive use in the most ordinary way, to print or display an
    announcement including an appropriate copyright notice and a
    notice that there is no warranty (or else, saying that you provide
    a warranty) and that users may redistribute the program under
    these conditions, and telling the user how to view a copy of this
    License.  (Exception: if the Program itself is interactive but
    does not normally print such an announcement, your work based on
    the Program is not required to print an announcement.)

These requirements apply to the modified work as a whole.  If
identifiable sections of that work are not derived from the Program,
and can be reasonably considered independent and separate works in
themselves, then this License, and its terms, do not apply to those
sections when you distribute them as separate works.  But when you
distribute the same sections as part of a whole which is a work based
on the Program, the distribution of the whole must be on the terms of
this License, whose permissions for other licensees extend to the
entire whole, and thus to each and every part regardless of who wrote it.

Thus, it is not the intent of this section to claim rights or contest
your rights to work written entirely by you; rather, the intent is to
exercise the right to control the distribution of derivative or
collective works based on the Program.

In addition, mere aggregation of another work not based on the Program
with the Program (or with a work based on the Program) on a volume of
a storage or distribution medium does not bring the other work under
the scope of this License.

  3. You may copy and distribute the Program (or a work based on it,
under Section 2) in object code or executable form under the terms of
Sections 1 and 2 above provided that you also do one of the following:

    a) Accompany it with the complete corresponding machine-readable
    source code, which must be distributed under the terms of Sections
    1 and 2 above on a medium customarily used for software interchange; or,

    b) Accompany it with a written offer, valid for at least three
    years, to give any third party, for a charge no more than your
    cost of physically performing source distribution, a complete
    machine-readable copy of the corresponding source code, to be
    distributed under the terms of Sections 1 and 2 above on a medium
    customarily used for software interchange; or,

    c) Accompany it with the information you received as to the offer
    to distribute corresponding source code.  (This alternative is
    allowed only for noncommercial distribution and only if you
    received the program in object code or executable form with such
    an offer, in accord with Subsection b above.)

The source code for a work means the preferred form of the work for
making modifications to it.  For an executable work, complete source
code means all the source code for all modules it contains, plus any
associated interface definition files, plus the scripts used to
control compilation and installation of the executable.  However, as a
special exception, the source code distributed need not include
anything that is normally distributed (in either source or binary
form) with the major components (compiler, kernel, and so on) of the
operating system on which the executable runs, unless that component
itself accompanies the executable.

In addition, as a special exception, the copyright holders give
permission to link the code of portions of this program with the
Julius library under certain conditions as described in each
individual source file, and distribute linked combinations
including the two.
You must obey the GNU General Public License in all respects
for all of the code used other than Julius.  If you modify
file(s) with this exception, you may extend this exception to your
version of the file(s), but you are not obligated to do so.  If you
do not wish to do so, delete this exception statement from your
version.  If you delete this exception statement from all source
files in the program, then also delete it here.

If distribution of executable or object code is made by offering
access to copy from a designated place, then offering equivalent
access to copy the source code from the same place counts as
distribution of the source code, even though third parties are not
compelled to copy the source along with the object code.

  4. You may not copy, modify, sublicense, or distribute the Program
except as expressly provided under this License.  Any attempt
otherwise to copy, modify, sublicense or distribute the Program is
void, and will automatically terminate your rights under this License.
However, parties who have received copies, or rights, from you under
this License will not have their licenses terminated so long as such
parties remain in full compliance.

  5. You are not required to accept this License, since you have not
signed it.  However, nothing else grants you permission to modify or
distribute the Program or its derivative works.  These actions are
prohibited by law if you do not accept this License.  Therefore, by
modifying or distributing the Program (or any work based on the
Program), you indicate your acceptance of this License to do so, and
all its terms and conditions for copying, distributing or modifying
the Program or works based on it.

  6. Each time you redistribute the Program (or any work based on the
Program), the recipient automatically receives a license from the
original licensor to copy, distribute or modify the Program subject to
these terms and conditions.  You may not impose any further
restrictions on the recipients' exercise of the rights granted herein.
You are not responsible for enforcing compliance by third parties to
this License.

  7. If, as a consequence of a court judgment or allegation of patent
infringement or for any other reason (not limited to patent issues),
conditions are imposed on you (whether by court order, agreement or
otherwise) that contradict the conditions of this License, they do not
excuse you from the conditions of this License.  If you cannot
distribute so as to satisfy simultaneously your obligations under this
License and any other pertinent obligations, then as a consequence you
may not distribute the Program at all.  For example, if a patent
license would not permit royalty-free redistribution of the Program by
all those who receive copies directly or indirectly through you, then
the only way you could satisfy both it and this License would be to
refrain entirely from distribution of the Program.

If any portion of this section is held invalid or unenforceable under
any particular circumstance, the balance of the section is intended to
apply and the section as a whole is intended to apply in other
circumstances.

It is not the purpose of this section to induce you to infringe any
patents or other property right claims or to contest validity of any
such claims; this section has the sole purpose of protecting the
integrity of the free software distribution system, which is
implemented by public license practices.  Many people have made
generous contributions to the wide range of software distributed
through that system in reliance on consistent application of that
system; it is up to the author/donor to decide if he or she is willing
to distribute software through any other system and a licensee cannot
impose that choice.

This section is intended to make thoroughly clear what is believed to
be a consequence of the rest of this License.

  8. If the distribution and/or use of the Program is restricted in
certain countries either by patents or by copyrighted interfaces, the
original copyright holder who places the Program under this License
may add an explicit geographical distribution limitation excluding
those countries, so that distribution is permitted only in or among
countries not thus excluded.  In such case, this License incorporates
the limitation as if written in the body of this License.

  9. The Free Software Foundation may publish revised and/or new versions
of the General Public License from time to time.  Such new versions will
be similar in spirit to the present version, but may differ in detail to
address new problems or concerns.

Each version is given a distinguishing version number.  If the Program
specifies a version number of this License which applies to it and "any
later version", you have the option of following the terms and conditions
either of that version or of any later version published by the Free
Software Foundation.  If the Program does not specify a version number of
this License, you may choose any version ever published by the Free Software
Foundation.

  10. If you wish to incorporate parts of the Program into other free
programs whose distribution conditions are different, write to the author
to ask for permission.  For software which is copyrighted by the Free
Software Foundation, write to the Free Software Foundation; we sometimes
make exceptions for this.  Our decision will be guided by the two goals
of preserving the free status of all derivatives of our free software and
of promoting the sharing and reuse of software generally.

			    NO WARRANTY

  11. BECAUSE THE PROGRAM IS LICENSED FREE OF CHARGE, THERE IS NO WARRANTY
FOR THE PROGRAM, TO THE EXTENT PERMITTED BY APPLICABLE LAW.  EXCEPT WHEN
OTHERWISE STATED IN WRITING THE COPYRIGHT HOLDERS AND/OR OTHER PARTIES
PROVIDE THE PROGRAM "AS IS" WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED
OR IMPLIED, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.  THE ENTIRE RISK AS
TO THE QUALITY AND PERFORMANCE OF THE PROGRAM IS WITH YOU.  SHOULD THE
PROGRAM PROVE DEFECTIVE, YOU ASSUME THE COST OF ALL NECESSARY SERVICING,
REPAIR OR CORRECTION.

  12. IN NO EVENT UNLESS REQUIRED BY APPLICABLE LAW OR AGREED TO IN WRITING
WILL ANY COPYRIGHT HOLDER, OR ANY OTHER PARTY WHO MAY MODIFY AND/OR
REDISTRIBUTE THE PROGRAM AS PERMITTED ABOVE, BE LIABLE TO YOU FOR DAMAGES,
INCLUDING ANY GENERAL, SPECIAL, INCIDENTAL OR CONSEQUENTIAL DAMAGES ARISING
OUT OF THE USE OR INABILITY TO USE THE PROGRAM (INCLUDING BUT NOT LIMITED
TO LOSS OF DATA OR DATA BEING RENDERED INACCURATE OR LOSSES SUSTAINED BY
YOU OR THIRD PARTIES OR A FAILURE OF THE PROGRAM TO OPERATE WITH ANY OTHER
PROGRAMS), EVEN IF SUCH HOLDER OR OTHER PARTY HAS BEEN ADVISED OF THE
POSSIBILITY OF SUCH DAMAGES.

		     END OF TERMS AND CONDITIONS

	    How to Apply These Terms to Your New Programs

  If you develop a new program, and you want it to be of the greatest
possible use to the public, the best way to achieve this is to make it
free software which everyone can redistribute and change under these terms.

  To do so, attach the following notices to the program.  It is safest
to attach them to the start of each source file to most effectively
convey the exclusion of warranty; and each file should have at least
the "copyright" line and a pointer to where the full notice is found.

    <one line to give the program's name and a brief idea of what it does.>
    Copyright (C) <year>  <name of author>

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.

Also add information on how to contact you by electronic and paper mail.

If the program is interactive, make it output a short notice like this
when it starts in an interactive mode:

    Gnomovision version 69, Copyright (C) year name of author
    Gnomovision comes with ABSOLUTELY NO WARRANTY; for details type `show w'.
    This is free software, and you are welcome to redistribute it
    under certain conditions; type `show c' for details.

The hypothetical commands `show w' and `show c' should show the appropriate
parts of the General Public License.  Of course, the commands you use may
be called something other than `show w' and `show c'; they could even be
mouse-clicks or menu items--whatever suits your program.

You should also get your employer (if you work as a programmer) or your
school, if any, to sign a "copyright disclaimer" for the program, if
necessary.  Here is a sample; alter the names:

  Yoyodyne, Inc., hereby disclaims all copyright interest in the program
  `Gnomovision' (which makes passes at compilers) written by James Hacker.

  <signature of Ty Coon>, 1 April 1989
  Ty Coon, President of Vice

This General Public License does not permit incorporating your program into
proprietary programs.  If your program is a subroutine library, you may
consider it more useful to permit linking proprietary applications with the
library.  If this is what you want to do, use the GNU Lesser General
Public License instead of this License.




================   Julius Licence   ==================


*** English translation is available in the latter of this file ***

	「大語彙連続音声認識エンジン Julius」
			利用許諾書

  Copyright (c)   1991-2010 京都大学 河原研究室
  Copyright (c)   1997-2000 情報処理振興事業協会(IPA)
  Copyright (c)   2000-2005 奈良先端科学技術大学院大学 鹿野研究室
  Copyright (c)   2005-2010 名古屋工業大学 Julius開発チーム

----------------------------------------------------------------------------

「大語彙連続音声認識エンジン Julius」（Julianを含む）は、京都大学 河原
研究室、奈良先端科学技術大学院大学 鹿野研究室、及び名古屋工業大学 
Julius開発チームで開発されています。1997年度から3年間、情報処理振興事
業協会(IPA)が実施した「独創的情報技術育成事業」の援助を受けました。

京都大学 河原研究室、IPA、奈良先端科学技術大学院大学 鹿野研究室、及び
名古屋工業大学 Julius開発チーム（以下あわせて「著作権者」と言う）は、
著作者であり著作権を留保していますが、本利用条件の全てを受諾し遵守する
限り、ソースコードを含む本プログラム及びドキュメンテーション（以下あわ
せて「本ソフトウェア」と言う）を無償であなたに提供します。あなたが本ソ
フトウェアを利用したときは、本利用条件の全てを受諾したものと看做されま
す。

			【利用条件】

1. あなたは、本利用条件の全てを受諾し遵守する限り、本ソフトウェアの全
部又は一部について使用、複製、翻案、変更、組込み、結合することおよびそ
れらの複製物、翻案物、変更物等を配布、送信することができます。ただし、
あなたを含め本ソフトウェアの利用者は、本ソフトウェアの全部又はその一部
を変更してその複製物を配布、送信などして第三者に提供するときは第2項の
表示記載に加え本ソフトウェアを変更した旨、変更者及びその変更日を明確に
表示するものとします。

2. あなたは、使用、複製、翻案、変更、組込み、結合その他本ソフトウェア
の利用態様の如何にかかわらず、その複製物、翻案物、変更物等の全部又は一
部を第三者に提供するときは、本ソフトウェアに下記の著作権表示及び公開の
趣旨を含む本利用条件の全て（この文書ファイル）をいささかも変更すること
なくそのまま表示し添付しなければなりません。

			記
  Copyright (c) 1991-2010 京都大学 河原研究室
  Copyright (c) 1997-2000 情報処理振興事業協会(IPA)
  Copyright (c) 2000-2005 奈良先端科学技術大学院大学 鹿野研究室
  Copyright (c) 2005-2010 名古屋工業大学 Julius開発チーム

3. 本ソフトウェアを利用して得られた知見に関して発表を行なう際には、
「大語彙連続音声認識エンジン Julius」を利用したことを明記して下さい。

4. 本ソフトウェアは、研究開発の試作物としてあるがままの状態で無償公開
提供するものであり、本ソフトウェアに関し、明示、黙示を問わず、いかなる
国における利用であるかを問わず、また法令により生じるものであるか否かを
問わず、一切の保証を行いません。ここで言う保証には、本ソフトウェアの品
質、性能、商品性、特定目的適合性、欠陥のないことおよび他の第三者の有す
る著作権、特許権、商標権等の無体財産権や営業秘密その他の権利利益を侵害
しないことの保証を含みますが、それに限定されるものではありません。あな
たを含め本ソフトウェアの利用者は、本ソフトウェアが無保証であることを承
諾し、本ソフトウェアが無保証であることのリスクを利用者自身で負うものと
します。裁判所の判決その他何らかの理由によりあなたに課せられた義務と本
利用条件が相容れないときは、本ソフトウェアを利用してはなりません。本ソ
フトウェアの利用又は利用できないことに関してあなた及び第三者に生じる通
常損害、特別損害、直接的、間接的、付随的、派生的な損害（逸失利益を含む）
一切につき、それが契約、不法行為責任、瑕疵担保責任、製造物責任等いかな
る国のいかなる法律原因によるかを問わず、賠償しません。

5. 本ソフトウェアの利用に関しては、日本国の法律を準拠法とし、京都地方
裁判所を第一審の専属管轄裁判所とします。

6. 本ソフトウェアのメンテナンスやサポート、上記条件以外の利用等に関し
ては、名古屋工業大学 Julius開発チーム、または京都大学 河原研究室に照会
下さい。



*** This is English translation of the Japanese original for reference ***


     Large Vocabulary Continuous Speech Recognition Engine Julius


 Copyright (c) 1997-2000 Information-technology Promotion Agency, Japan
 Copyright (c) 1991-2010 Kawahara Lab., Kyoto University
 Copyright (c) 2000-2005 Shikano Lab., Nara Institute of Science and Technology
 Copyright (c) 2005-2010 Julius project team, Nagoya Institute of Technology

"Large Vocabulary Continuous Speech Recognition Engine Julius",
including Julian, is being developed at Kawahara Lab., Kyoto
University, Shikano Lab., Nara Institute of Science and Technology,
and Julius project team, Nagoya Institute of Technology (collectively
referred to herein as the "Licensers").  Julius was funded by the
Advanced Information Technology Program Project of
Information-technology Promotion Agency (IPA), Japan for three years
since 1997.

The Licensers reserve the copyright thereto.  However, as long as you
accept and remain in strict compliance with the terms and conditions
of the license set forth herein, you are hereby granted a royalty-free
license to use "Large Vocabulary Continuous Speech Recognition Engine
Julius" including the source code thereof and the documentation
thereto (collectively referred to herein as the "Software").  Use by
you of the Software shall constitute acceptance by you of all terms
and conditions of the license set forth herein.

	      TERMS AND CONDITIONS OF LICENSE

1. So long as you accept and strictly comply with the terms and
conditions of the license set forth herein, the Licensers will not
enforce the copyright or moral rights in respect of the Software, in
connection with the use, copying, duplication, adaptation,
modification, preparation of a derivative work, aggregation with
another program, or insertion into another program of the Software or
the distribution or transmission of the Software.  However, in the
event you or any other user of the Software revises all or any portion
of the Software, and such revision is distributed, then, in addition
to the notice required to be affixed pursuant to paragraph 2 below, a
notice shall be affixed indicating that the Software has been revised,
and indicating the date of such revision and the name of the person or
entity that made the revision.

2. In the event you provide to any third party all or any portion of
the Software, whether for copying, duplication, adaptation,
modification, preparation of a derivative work, aggregation with
another program, insertion into another program, or other use, you
shall affix the following copyright notice and all terms and
conditions of this license (both the Japanese original and English
translation) as set forth herein, without any revision or change
whatsoever.

                      Form of copyright notice:

 Copyright (c) 1997-2000 Information-technology Promotion Agency, Japan
 Copyright (c) 1991-2010 Kawahara Lab., Kyoto University
 Copyright (c) 2000-2005 Shikano Lab., Nara Institute of Science and Technology
 Copyright (c) 2005-2010 Julius project team, Nagoya Institute of Technology

3. When you publish or present any results by using the Software, you
must explicitly mention your use of "Large Vocabulary Continuous
Speech Recognition Engine Julius".

4. The Licensers are licensing the Software, which is the trial
product of research and project, on an "as is" and royalty-free basis,
and makes no warranty or guaranty whatsoever with respect to the
Software, whether express or implied, irrespective of the nation where
used, and whether or not arising out of statute or otherwise,
including but not limited to any warranty or guaranty with respect to
quality, performance, merchantability, fitness for a particular
purpose, absence of defects, or absence of infringement of copyright,
patent rights, trademark rights or other intellectual property rights,
trade secrets or proprietary rights of any third party.  You and every
other user of the Software hereby acknowledge that the Software is
licensed without any warranty or guaranty, and assume all risks
arising out of the absence of any warranty or guaranty.  In the event
the terms and conditions of this license are inconsistent with the
obligations imposed upon you by judgment of a court or for any other
reason, you may not use the Software.

The Licensers shall not have any liability to you or to any third
party for damages or liabilities of any nature whatsoever arising out
of your use of or inability to use the Software, whether of an
ordinary, special, direct, indirect, consequential or incidental
nature (including without limitation lost profits) or otherwise, and
whether arising out of contract, negligence, tortuous conduct, product
liability or any other legal theory or reason whatsoever of any nation
or jurisdiction.

5. This license of use of the Software shall be governed by the laws
of Japan, and the Kyoto District Court shall have exclusive primary
jurisdiction with respect to all disputes arising with respect
thereto.

6. Inquiries for support or maintenance of the Software, or inquiries
concerning this license of use besides the conditions above, may be
sent to Julius project team, Nagoya Institute of Technology, or
Kawahara Lab., Kyoto University.
