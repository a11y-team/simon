<?xml version="1.0" ?>
<!DOCTYPE book PUBLIC "-//KDE//DTD DocBook XML V4.2-Based Variant V1.1//EN" "dtd/kdex.dtd" [
  <!-- Define an entity for your application if it is not part of KDE
       CVS -->
  <!ENTITY kmyapplication "<application
>ksimond</application
>">
  <!ENTITY kappname "&kmyapplication;"
><!-- replace kmyapplication here
                                            do *not* replace kappname-->
  <!ENTITY package "kde-module"
><!-- kdebase, kdeadmin, etc.  Leave
                                     this unchanged if your
                                     application is not maintained in KDE CVS -->
  <!ENTITY % addindex "IGNORE">
  <!ENTITY % English "INCLUDE"
> <!-- ONLY If you are writing non-English
                                     original documentation, change
                                     the language here -->

  <!-- Do not define any other entities; instead, use the entities
       from entities/general.entities and $LANG/user.entities. -->
]>
<!-- kdoctemplate v0.9 January 10 2003
     Changes to comments to clarify entity usage January 10 2003
     Minor update to "Credits and Licenses" section on August 24, 2000
     Removed "Revision history" section on 22 January 2001
     Changed to Installation/Help menu entities 18 October 2001
     Other minor cleanup and changes 18 October 2001
     FPI change and minor changes November 2002 -->

<!--
This template was designed by: David Rugge davidrugge@mindspring.com
with lots of help from: Eric Bischoff ebisch@cybercable.tm.fr
and Frederik Fouvry fouvry@sfs.nphil.uni-tuebingen.de
of the KDE DocBook team.

You may freely use this template for writing any sort of KDE documentation.
If you have any changes or improvements, please let us know.

Remember:
- in XML, the case of the <tags
> and attributes is relevant ;
- also, quote all attributes.

Please don't forget to remove all these comments in your final documentation,
thanks ;-).
-->

<!-- ................................................................ -->

<!-- The language must NOT be changed here. -->
<!-- If you are writing original documentation in a language other -->
<!-- than English, change the language above ONLY, not here -->
<book lang="&language;">

<!-- This header contains all of the meta-information for the document such
as Authors, publish date, the abstract, and Keywords -->

<bookinfo>

<othercredit role="translator"
><firstname
>Peter</firstname
><surname
>Grasch</surname
><affiliation
><address
><email
>grasch@simon-listens.org</email
></address
></affiliation
><contrib
>Deutsche Übersetzung</contrib
></othercredit
> 


<!-- Date and version information of the application
Don't forget to include this last date and this last revision number, we
need them for translation coordination !
Please respect the format of the date (YYYY-MM-DD) and of the version
(V.MM.LL), it could be used by automation scripts.
Do NOT change these in the translation. -->

<date
>2010-08-11</date>
<releaseinfo
>0.3</releaseinfo>

<!-- Abstract about this handbook -->

<abstract>
<para
>&kmyapplication; ist ein Frontend für die Serverkomponente simond der simon Spracherkennungslösung </para>
</abstract>

<!-- This is a set of Keywords for indexing by search engines.
Please at least include KDE, the KDE package it is in, the name
 of your application, and a few relevant keywords. -->

<keywordset>
<keyword
>KDE</keyword>
<keyword
>kdeutils</keyword>
<keyword
>Kapp</keyword>
<keyword
>simon</keyword>
<keyword
>Erkennung</keyword>
<keyword
>Sprache</keyword>
<keyword
>Sprecher</keyword>
<keyword
>Kommando</keyword>
<keyword
>Kontrolle</keyword>
<keyword
>Zugangshilfe</keyword>
</keywordset>

<!-- Translators: put here the copyright notice of the translation -->
<!-- Put here the FDL notice.  Read the explanation in fdl-notice.docbook
     and in the FDL itself on how to use it. -->
<legalnotice
>&FDLNotice;</legalnotice>

<copyright>
<year
>2009-2010</year>
<holder
>Peter Grasch</holder>
</copyright>

<authorgroup>
<author
><personname
> <firstname
>Peter</firstname
> <othername
>H.</othername
> <surname
>Grasch</surname
> </personname
> <email
>grasch@simon-listens.org</email
> </author>
</authorgroup>


<title
>Das &kmyapplication; Handbuch</title>
</bookinfo>

<chapter id="introduction">
<title
>Einleitung</title>

<para
>&kappname; ist ein einfaches, grafisches Frontend von simond. Es zeigt ein Symbol im Systembereich der Taskleiste an, von dem aus man den simond Server starten, stoppen und konfigurieren kann. Im Konfigurationsdialog von ksimond werden auch die simond Konfigurationselement angezeigt. </para>
<para
>KSimond kann so konfiguriert werden, dass es beim Start des Systems startet. </para>
<para
>Bitte melden Sie Probleme oder Funktionsanfragen auf unserem <ulink url="http://sourceforge.net/tracker/?group_id=190872"
>Bug Tracker</ulink
>. </para>
</chapter>

<chapter id="using-kapp">
<title
>&kappname; benutzen</title>

<para
>&kappname; hat kein Applikationsfenster. Stattdessen erzeugt es ein Symbol im Systembereich der Taskleiste. Über das Kontextmenü des Symbols kann simond gestartet und gestoppt werden und die Konfiguration angezeigt werden. <screenshot>
<screeninfo
>Screenshot von &kappname;</screeninfo>
	<mediaobject>
	  <imageobject>
	    <imagedata fileref="ksimond.png" format="PNG"/>
	  </imageobject>
	  <textobject>
	    <phrase
>&kappname; "Screenshot"</phrase>
	  </textobject>
	</mediaobject>
</screenshot>
</para>

<para
>ksimond kann konfiguriert werden um automatisch zu starten wenn KDE startet und auf simond Abstürze zu reagieren. <screenshot>
<screeninfo
>Screenshot von der &kappname;-Konfiguration</screeninfo>
  <mediaobject>
    <imageobject>
      <imagedata fileref="ksimond_config.png" format="PNG"/>
    </imageobject>
    <textobject>
      <phrase
>ksimond Konfiguration</phrase>
    </textobject>
  </mediaobject>
</screenshot>
</para>
<para
>Die Option ksimond beim Anmelden zu starten wird unter Microsoft Windows und KDE unter Linux funktionieren. Andere Desktopumgebungen wie GNOME, XFCE, etc. werden wahrscheinlich manuelle Platzierung von ksimond im jeweiligen Sitzungsautostart benötigen (bitte sehen Sie das jeweilige Handbuch der Desktopumgebung). </para>

<para
>Neben der ksimond Konfiguration werden auch die simond Konfigurationsmodule angezeigt (diese sind z.B. auch über KDEs systemsettings verfügbar). Sehen Sie das <ulink url="help:/simond"
>simond</ulink
> Handbuch für Details. <screenshot>
<screeninfo
>Screenshot der simond Benutzerkonfiguration</screeninfo>
  <mediaobject>
    <imageobject>
      <imagedata fileref="simond_user_config.png" format="PNG"/>
    </imageobject>
    <textobject>
      <phrase
>simond Benutzerkonfiguration</phrase>
    </textobject>
  </mediaobject>
</screenshot>

<screenshot>
<screeninfo
>Screenshot der simond Netzwerkkonfiguration</screeninfo>
  <mediaobject>
    <imageobject>
      <imagedata fileref="simond_network_config.png" format="PNG"/>
    </imageobject>
    <textobject>
      <phrase
>simond Netzwerkkonfiguration</phrase>
    </textobject>
  </mediaobject>
</screenshot>

<screenshot>
<screeninfo
>Bildschirmfoto der Sprachmodellkonfiguration</screeninfo>
  <mediaobject>
    <imageobject>
      <imagedata fileref="simond_speech_model_config.png" format="PNG"/>
    </imageobject>
    <textobject>
      <phrase
>simond Sprachmodellkonfiguration</phrase>
    </textobject>
  </mediaobject>
</screenshot>
</para>

</chapter>


<chapter id="credits">

<title
>Danksagungen und Lizenz</title>

<para
>&kappname; </para>
<para
>Program copyright 2008-2010 Peter Grasch <email
>grasch@simon-listens.org</email
> </para>
<!--<para>
Contributors:
<itemizedlist>
<listitem
><para
>Konqui the KDE Dragon <email
>konqui@kde.org</email
></para>
</listitem>
<listitem
><para
>Tux the Linux Penguin <email
>tux@linux.org</email
></para>
</listitem>
</itemizedlist>
</para
>-->

<para
>Documentation Copyright &copy; 2010 Peter Grasch <email
>grasch@simon-listens.org</email
> </para>

<para
>Übersetzung Copyright &copy; 2009 Peter Grasch <email
>grasch@simon-listens.org</email
></para
> 
&underFDL; &underGPL; </chapter>

<appendix id="installation">
<title
>Installation</title>

<sect1 id="getting-kapp">
<title
>&kappname; herunterladen</title>

<para
>Laden Sie die letzte Version von ksimond im simon-Paket von <ulink url="http://sourceforge.net/projects/speech2text"
>Sourceforge</ulink
> herunter. </para>
</sect1>

<sect1 id="requirements">
<title
>Voraussetzungen</title>

<para
>Um &kappname; zu benutzen benötigen Sie &kde; 4 und simond. </para>

<para
>simon und simond können auf der <ulink url="http://sourceforge.net/projects/speech2text"
>simon Sourceforge Seite</ulink
> finden. </para>

</sect1>

<sect1 id="compilation">
<title
>Kompilierung und Installation</title>

<para
>Bitte sehen Sie unser <ulink url="http://www.simon-listens.org/wiki/index.php/English:_Setup"
>online Wiki</ulink
> for eine detaillierte Installationsanleitung.</para>
</sect1>

</appendix>

&documentation.index;
</book>

<!--
Local Variables:
mode: xml
sgml-minimize-attributes:nil
sgml-general-insert-case:lower
sgml-indent-step:0
sgml-indent-data:nil
End:

vim:tabstop=2:shiftwidth=2:expandtab
kate: space-indent on; indent-width 2; tab-width 2; indent-mode none;
-->
