# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"Report-Msgid-Bugs-To: http://sourceforge.net/tracker/?"
"atid=935103&group_id=190872\n"
"POT-Creation-Date: 2010-07-16 20:21+0200\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=CHARSET\n"
"Content-Transfer-Encoding: 8bit\n"

#. i18n: file: config/ksimondconfiguration.kcfg:11
#. i18n: ectx: label, entry (AutoStart), group (KSimond)
#: po/rc.cpp:3 rc.cpp:3
msgid "should ksimond start on KDE start?"
msgstr ""

#. i18n: file: config/ksimondconfiguration.kcfg:13
#. i18n: ectx: tooltip, entry (AutoStart), group (KSimond)
#: po/rc.cpp:6 rc.cpp:6
msgid "Should ksimond start on KDE start?"
msgstr ""

#. i18n: file: config/ksimondconfiguration.kcfg:16
#. i18n: ectx: label, entry (AutoStartSimond), group (KSimond)
#: po/rc.cpp:9 rc.cpp:9
msgid "whether we want to start simond automatically."
msgstr ""

#. i18n: file: config/ksimondconfiguration.kcfg:18
#. i18n: ectx: tooltip, entry (AutoStartSimond), group (KSimond)
#: po/rc.cpp:12 rc.cpp:12
msgid "Whether we want to start simond automatically."
msgstr ""

#. i18n: file: config/ksimondconfiguration.kcfg:21
#. i18n: ectx: label, entry (AutoReStartSimond), group (KSimond)
#: po/rc.cpp:15 rc.cpp:15
msgid ""
"whether we want to start simond automatically after it quit unexpectedly."
msgstr ""

#. i18n: file: config/ksimondconfiguration.kcfg:23
#. i18n: ectx: tooltip, entry (AutoReStartSimond), group (KSimond)
#: po/rc.cpp:18 rc.cpp:18
msgid ""
"Whether we want to start simond automatically after it quit unexpectedly."
msgstr ""

#. i18n: file: config/ksimondconfiguration.ui:20
#. i18n: ectx: property (text), widget (QCheckBox, kcfg_AutoStart)
#: po/rc.cpp:21 rc.cpp:21
msgid "Start ksimond when logging in"
msgstr ""

#. i18n: file: config/ksimondconfiguration.ui:27
#. i18n: ectx: property (text), widget (QCheckBox, kcfg_AutoStartSimond)
#: po/rc.cpp:24 rc.cpp:24
msgid "Start simond when ksimond starts"
msgstr ""

#. i18n: file: config/ksimondconfiguration.ui:34
#. i18n: ectx: property (text), widget (QCheckBox, kcfg_AutoReStartSimond)
#: po/rc.cpp:27 rc.cpp:27
msgid "Restart simond if it quits unexpectedly"
msgstr ""

#: po/rc.cpp:28 rc.cpp:28
msgctxt "NAME OF TRANSLATORS"
msgid "Your names"
msgstr ""

#: po/rc.cpp:29 rc.cpp:29
msgctxt "EMAIL OF TRANSLATORS"
msgid "Your emails"
msgstr ""

#: src/ksimondview.cpp:54
msgid "simond"
msgstr ""

#: src/ksimondview.cpp:57 src/ksimondview.cpp:85
msgid "Start simond"
msgstr ""

#: src/ksimondview.cpp:62 src/ksimondview.cpp:84
msgid "Start simon"
msgstr ""

#: src/ksimondview.cpp:67 src/ksimondview.cpp:86
msgid "Restart simond"
msgstr ""

#: src/ksimondview.cpp:73 src/ksimondview.cpp:87
msgid "Stop simond"
msgstr ""

#: src/ksimondview.cpp:79 src/ksimondview.cpp:88
msgid "Configuration"
msgstr ""

#: src/ksimondview.cpp:117
msgid "simond is already running"
msgstr ""

#: src/ksimondview.cpp:138
msgid "simond stopped"
msgstr ""

#: src/ksimondview.cpp:153
msgid "simond started"
msgstr ""

#: src/ksimondview.cpp:181
msgid ""
"Could not start simond.\n"
"\n"
"Please check your configuration.\n"
"\n"
"Command: \"%1\""
msgstr ""

#: src/ksimondview.cpp:185
msgid "simond crashed. (Status: %1)"
msgstr ""

#: src/ksimondview.cpp:189
msgid "Timeout."
msgstr ""

#: src/ksimondview.cpp:192
msgid "Could not communicate with simond: Write failed."
msgstr ""

#: src/ksimondview.cpp:195
msgid "Could not communicate with simond: Read failed."
msgstr ""

#: src/ksimondview.cpp:198
msgid "Unknown Error"
msgstr ""

#: src/main.cpp:28
msgid "A KDE 4 frontend for simond"
msgstr ""

#: src/main.cpp:32
msgid "ksimond"
msgstr ""

#: src/main.cpp:33
msgid "(C) 2008 Peter Grasch"
msgstr ""

#: src/main.cpp:34
msgid "Peter Grasch"
msgstr ""
