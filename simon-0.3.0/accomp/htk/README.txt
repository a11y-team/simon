HTK is not part of this distribution as the licence forbids direct redistribution.

Please go to http://htk.eng.cam.ac.uk/register.shtml and register a HTK user account to receive the password needed for the download.

You will find sourcecode releases for Windows and Linux releases here: http://htk.eng.cam.ac.uk/download.shtml

Windows users can get an outdated but binary release here: http://htk.eng.cam.ac.uk/ftp/software/htk-3.3-windows-binary.zip

Without HTK, simon (simond) will not be able to compile an acoustic model which means you can not create or modify your speech model.
