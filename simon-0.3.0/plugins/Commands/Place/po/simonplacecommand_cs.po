# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the PACKAGE package.
#
# Pavel Fric <pavelfric@seznam.cz>, 2009, 2010.
msgid ""
msgstr ""
"Project-Id-Version: simonplacecommand\n"
"Report-Msgid-Bugs-To: http://sourceforge.net/tracker/?"
"atid=935103&group_id=190872\n"
"POT-Creation-Date: 2010-07-16 20:21+0200\n"
"PO-Revision-Date: 2010-06-25 11:19+0200\n"
"Last-Translator: Pavel\n"
"Language-Team: Czech <kde-i18n-doc@kde.org>\n"
"Language: cs\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Lokalize 1.0\n"
"Plural-Forms: nplurals=3; plural=(n==1) ? 0 : (n>=2 && n<=4) ? 1 : 2;\n"

#: placecommand.cpp:32
msgid "Place"
msgstr "Umístění"

#: placecommand.cpp:57
msgid "URL"
msgstr "Internetová adresa"

#. i18n: file: createplacecommandwidget.ui:20
#. i18n: ectx: property (text), widget (QCommandLinkButton, cbImportPlace)
#: po/rc.cpp:3 rc.cpp:3
msgid "Select place"
msgstr "Vybrat umístění"

#. i18n: file: createplacecommandwidget.ui:23
#. i18n: ectx: property (description), widget (QCommandLinkButton, cbImportPlace)
#: po/rc.cpp:6 rc.cpp:6
msgid ""
"This dialog will help you to specify the needed options to select both local "
"or remote places"
msgstr ""
"Tento dialog vám pomůže se zadáním voleb potřebných pro výběr místního nebo "
"vzdáleného umístění"

#. i18n: file: createplacecommandwidget.ui:30
#. i18n: ectx: property (text), widget (QCommandLinkButton, commandLinkButton_3)
#: po/rc.cpp:9 rc.cpp:9
#, fuzzy
msgctxt "Input the information manually (instead of automatically)"
msgid "Manual"
msgstr "Ruční"

#. i18n: file: createplacecommandwidget.ui:36
#. i18n: ectx: property (description), widget (QCommandLinkButton, commandLinkButton_3)
#: po/rc.cpp:12 rc.cpp:12
msgid "Configure the necessairy parameters manually"
msgstr "Nezbytné proměnné nastavit ručně"

#. i18n: file: createplacecommandwidget.ui:48
#. i18n: ectx: property (text), widget (QLabel, label_14)
#. i18n: file: selectplacedlg.ui:110
#. i18n: ectx: property (text), widget (QLabel, lbRemoteUrl)
#. i18n: file: createplacecommandwidget.ui:48
#. i18n: ectx: property (text), widget (QLabel, label_14)
#. i18n: file: selectplacedlg.ui:110
#. i18n: ectx: property (text), widget (QLabel, lbRemoteUrl)
#: po/rc.cpp:15 po/rc.cpp:27 rc.cpp:15 rc.cpp:27
msgid "URL:"
msgstr "Internetová adresa:"

#. i18n: file: selectplacedlg.ui:19
#. i18n: ectx: property (text), widget (QRadioButton, rbLocalPlace)
#: po/rc.cpp:18 rc.cpp:18
msgid "Local Place"
msgstr "Místní umístění"

#. i18n: file: selectplacedlg.ui:50
#. i18n: ectx: property (text), widget (QRadioButton, rbLocalFile)
#: po/rc.cpp:21 rc.cpp:21
msgid "Local File"
msgstr "Místní soubor"

#. i18n: file: selectplacedlg.ui:81
#. i18n: ectx: property (text), widget (QRadioButton, rbRemotePlace)
#: po/rc.cpp:24 rc.cpp:24
msgid "Remote URL"
msgstr "Vzdálená internetová adresa"

#. i18n: file: selectplacedlg.ui:126
#. i18n: ectx: property (title), widget (QGroupBox, gbRemoteHelp)
#: po/rc.cpp:30 rc.cpp:30
msgid "Help"
msgstr "Nápověda"

#. i18n: file: selectplacedlg.ui:141
#. i18n: ectx: property (text), widget (QLabel, lbProtocol)
#: po/rc.cpp:33 rc.cpp:33
msgid "Protocol:"
msgstr "Zpráva:"

#. i18n: file: selectplacedlg.ui:152
#. i18n: ectx: property (text), item, widget (KComboBox, cbProtocol)
#: po/rc.cpp:36 rc.cpp:36
msgid "http"
msgstr "http"

#. i18n: file: selectplacedlg.ui:157
#. i18n: ectx: property (text), item, widget (KComboBox, cbProtocol)
#: po/rc.cpp:39 rc.cpp:39
msgid "https"
msgstr "https"

#. i18n: file: selectplacedlg.ui:162
#. i18n: ectx: property (text), item, widget (KComboBox, cbProtocol)
#: po/rc.cpp:42 rc.cpp:42
msgid "ftp"
msgstr "ftp"

#. i18n: file: selectplacedlg.ui:167
#. i18n: ectx: property (text), item, widget (KComboBox, cbProtocol)
#: po/rc.cpp:45 rc.cpp:45
msgid "sftp"
msgstr "sftp"

#. i18n: file: selectplacedlg.ui:172
#. i18n: ectx: property (text), item, widget (KComboBox, cbProtocol)
#: po/rc.cpp:48 rc.cpp:48
msgid "smb"
msgstr "smb"

#. i18n: file: selectplacedlg.ui:180
#. i18n: ectx: property (text), widget (QCheckBox, cbAuthentification)
#: po/rc.cpp:51 rc.cpp:51
msgid "Use Authentication"
msgstr "Použít potvrzení pravosti"

#. i18n: file: selectplacedlg.ui:190
#. i18n: ectx: property (text), widget (QLabel, lbUser)
#: po/rc.cpp:54 rc.cpp:54
msgid "User:"
msgstr "Uživatel:"

#. i18n: file: selectplacedlg.ui:207
#. i18n: ectx: property (text), widget (QLabel, lbPassword)
#: po/rc.cpp:57 rc.cpp:57
msgid "Pass:"
msgstr "Propustnost:"

#. i18n: file: selectplacedlg.ui:224
#. i18n: ectx: property (text), widget (QLabel, lbHost)
#: po/rc.cpp:60 rc.cpp:60
msgid "Host:"
msgstr "Hostitelský počítač"

#. i18n: file: selectplacedlg.ui:234
#. i18n: ectx: property (text), widget (QLabel, lbPath)
#: po/rc.cpp:63 rc.cpp:63
msgid "Path:"
msgstr "Cesta:"

#: po/rc.cpp:64 rc.cpp:64
msgctxt "NAME OF TRANSLATORS"
msgid "Your names"
msgstr "Pavel Fric"

#: po/rc.cpp:65 rc.cpp:65
msgctxt "EMAIL OF TRANSLATORS"
msgid "Your emails"
msgstr "pavelfric@seznam.cz"

#: selectplacedialog.cpp:30
msgid "Select Place"
msgstr "Vybrat umístění"

#: selectplacedialog.cpp:68
msgid "Open local path: %1"
msgstr "Otevřít místní cestu: %1"

#: selectplacedialog.cpp:72
msgid "Open local file: %1"
msgstr "Otevřít místní soubor: %1"

#: selectplacedialog.cpp:76
msgid "Open remote URL: %1"
msgstr "Otevřít vzdálenou internetovou adresu (URL): %1"

#~ msgid "Form"
#~ msgstr "Formulář"

#~ msgid "Add Place"
#~ msgstr "Přidat umístění"

#~ msgid "Add a new Place"
#~ msgstr "Přidat nové umístění"

#~ msgid ""
#~ "With this wizard you can assoziate places with commands.\n"
#~ "\n"
#~ "Both local and remote places are supported."
#~ msgstr ""
#~ "S pomocí tohoto kouzelníka můžete asociovat umístění s příkazy.\n"
#~ "\n"
#~ "Jsou podporována jak místní tak vzdálená umístění."

#~ msgid "Place added"
#~ msgstr "Bylo přidáno umístění"

#~ msgid "Press Finish to complete this wizard."
#~ msgstr "Stiskněte \"Dokončit\" pro dokončení tohoto kouzelníka."

#~ msgid "Import Place"
#~ msgstr "Zavést umístění"

#~ msgid "Using this wizard, you can import local and remote places easily."
#~ msgstr ""
#~ "S pomocí tohoto kouzelníka můžete jednoduše zavést místní a vzdálená "
#~ "umístění."

#~ msgid "[INF] Loading place commands from %1"
#~ msgstr "[INF] Nahrává příkazy spojené s umístěními z %1"

#~ msgid "[INF] Saving place commands to %1"
#~ msgstr "[INF] Ukládá příkazy spojené s umístěními do %1"
