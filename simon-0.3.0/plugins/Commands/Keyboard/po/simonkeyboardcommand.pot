# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"Report-Msgid-Bugs-To: http://sourceforge.net/tracker/?"
"atid=935103&group_id=190872\n"
"POT-Creation-Date: 2010-07-16 20:21+0200\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=CHARSET\n"
"Content-Transfer-Encoding: 8bit\n"

#: keyboardcommandmanager.cpp:55
msgid "Activate Keyboard"
msgstr ""

#. i18n: file: keyboardwidget.ui:14
#. i18n: ectx: property (windowTitle), widget (QWidget, KeyboardDlg)
#: keyboardcommandmanager.cpp:114 keyboardcommandmanager.cpp:377
#: keyboardconfiguration.cpp:43 po/rc.cpp:54 rc.cpp:54
msgid "Keyboard"
msgstr ""

#: keyboardcommandmanager.cpp:342 keyboardcommandmanager.cpp:413
msgctxt "Number"
msgid "Zero"
msgstr ""

#: keyboardcommandmanager.cpp:342 keyboardcommandmanager.cpp:416
msgctxt "Number"
msgid "One"
msgstr ""

#: keyboardcommandmanager.cpp:342 keyboardcommandmanager.cpp:419
msgctxt "Number"
msgid "Two"
msgstr ""

#: keyboardcommandmanager.cpp:343 keyboardcommandmanager.cpp:422
msgctxt "Number"
msgid "Three"
msgstr ""

#: keyboardcommandmanager.cpp:343 keyboardcommandmanager.cpp:425
msgctxt "Number"
msgid "Four"
msgstr ""

#: keyboardcommandmanager.cpp:343 keyboardcommandmanager.cpp:428
msgctxt "Number"
msgid "Five"
msgstr ""

#: keyboardcommandmanager.cpp:344 keyboardcommandmanager.cpp:431
msgctxt "Number"
msgid "Six"
msgstr ""

#: keyboardcommandmanager.cpp:344 keyboardcommandmanager.cpp:434
msgctxt "Number"
msgid "Seven"
msgstr ""

#: keyboardcommandmanager.cpp:344 keyboardcommandmanager.cpp:437
msgctxt "Number"
msgid "Eight"
msgstr ""

#: keyboardcommandmanager.cpp:344 keyboardcommandmanager.cpp:440
msgctxt "Number"
msgid "Nine"
msgstr ""

#: keyboardcommandmanager.cpp:378
msgid "Starts the keyboard"
msgstr ""

#: keyboardcommandmanager.cpp:384
msgctxt "Close the dialog"
msgid "Ok"
msgstr ""

#: keyboardcommandmanager.cpp:385
msgid "Hides the keyboard"
msgstr ""

#: keyboardcommandmanager.cpp:387
msgctxt "Keyboard key"
msgid "Shift"
msgstr ""

#: keyboardcommandmanager.cpp:388
msgid "Shift key"
msgstr ""

#: keyboardcommandmanager.cpp:390
msgctxt "Keyboard key"
msgid "Caps lock"
msgstr ""

#: keyboardcommandmanager.cpp:391
msgid "Caps lock key"
msgstr ""

#: keyboardcommandmanager.cpp:393
msgctxt "Keyboard key"
msgid "Control"
msgstr ""

#. i18n: file: keyboardwidget.ui:169
#. i18n: ectx: property (text), widget (KPushButton, pbControl)
#: keyboardcommandmanager.cpp:394 po/rc.cpp:102 rc.cpp:102
msgid "Control"
msgstr ""

#: keyboardcommandmanager.cpp:396
msgctxt "Keyboard key"
msgid "Backspace"
msgstr ""

#. i18n: file: keyboardwidget.ui:202
#. i18n: ectx: property (text), widget (KPushButton, pbBackspace)
#: keyboardcommandmanager.cpp:397 po/rc.cpp:111 rc.cpp:111
msgid "Backspace"
msgstr ""

#: keyboardcommandmanager.cpp:399
msgctxt "Keyboard key"
msgid "Return"
msgstr ""

#. i18n: file: keyboardwidget.ui:235
#. i18n: ectx: property (text), widget (KPushButton, pbReturn)
#: keyboardcommandmanager.cpp:400 po/rc.cpp:117 rc.cpp:117
msgid "Return"
msgstr ""

#: keyboardcommandmanager.cpp:402
msgctxt "Keyboard key"
msgid "Alt"
msgstr ""

#. i18n: file: keyboardwidget.ui:242
#. i18n: ectx: property (text), widget (KPushButton, pbAlt)
#: keyboardcommandmanager.cpp:403 po/rc.cpp:120 rc.cpp:120
msgid "Alt"
msgstr ""

#: keyboardcommandmanager.cpp:405
msgctxt "Keyboard key"
msgid "AltGr"
msgstr ""

#. i18n: file: keyboardwidget.ui:252
#. i18n: ectx: property (text), widget (KPushButton, pbAltGr)
#: keyboardcommandmanager.cpp:406 po/rc.cpp:123 rc.cpp:123
msgid "AltGr"
msgstr ""

#: keyboardcommandmanager.cpp:408
msgctxt "Keyboard key"
msgid "Super"
msgstr ""

#. i18n: file: keyboardwidget.ui:262
#. i18n: ectx: property (text), widget (KPushButton, pbSuper)
#: keyboardcommandmanager.cpp:409 po/rc.cpp:126 rc.cpp:126
msgctxt "Super key"
msgid "Super"
msgstr ""

#: keyboardcommandmanager.cpp:414
msgid "Clicks 0"
msgstr ""

#: keyboardcommandmanager.cpp:417
msgid "Clicks 1"
msgstr ""

#: keyboardcommandmanager.cpp:420
msgid "Clicks 2"
msgstr ""

#: keyboardcommandmanager.cpp:423
msgid "Clicks 3"
msgstr ""

#: keyboardcommandmanager.cpp:426
msgid "Clicks 4"
msgstr ""

#: keyboardcommandmanager.cpp:429
msgid "Clicks 5"
msgstr ""

#: keyboardcommandmanager.cpp:432
msgid "Clicks 6"
msgstr ""

#: keyboardcommandmanager.cpp:435
msgid "Clicks 7"
msgstr ""

#: keyboardcommandmanager.cpp:438
msgid "Clicks 8"
msgstr ""

#: keyboardcommandmanager.cpp:441
msgid "Clicks 9"
msgstr ""

#: keyboardcommandmanager.cpp:443
msgctxt "Decimal separator (voice trigger)"
msgid "Point"
msgstr ""

#: keyboardcommandmanager.cpp:444
msgid "Decimal separator"
msgstr ""

#. i18n: file: keyboardwidget.ui:138
#. i18n: ectx: property (text), widget (KPushButton, pbSelectNumber)
#: keyboardcommandmanager.cpp:447 keyboardcommandmanager.cpp:448 po/rc.cpp:96
#: rc.cpp:96
msgid "Select number"
msgstr ""

#. i18n: file: keyboardwidget.ui:145
#. i18n: ectx: property (text), widget (KPushButton, pbWriteOutNumber)
#: keyboardcommandmanager.cpp:450 keyboardcommandmanager.cpp:451 po/rc.cpp:99
#: rc.cpp:99
msgid "Write number"
msgstr ""

#: keyboardcommandmanager.cpp:453 keyboardcommandmanager.cpp:454
msgid "Number backspace"
msgstr ""

#: keyboardconfiguration.cpp:44
msgid "Input signes with ease"
msgstr ""

#: keyboardconfiguration.cpp:128
msgid "Add keyboard set"
msgstr ""

#: keyboardconfiguration.cpp:128
msgid "Please enter the name of the new set:"
msgstr ""

#: keyboardconfiguration.cpp:131
msgid "Failed to add set"
msgstr ""

#: keyboardconfiguration.cpp:145
msgid "Please choose a set to be edited"
msgstr ""

#: keyboardconfiguration.cpp:151
msgid "Edit keyboard set"
msgstr ""

#: keyboardconfiguration.cpp:151
msgid "Please enter the new name of the set:"
msgstr ""

#: keyboardconfiguration.cpp:154
msgid "Failed to edit set"
msgstr ""

#: keyboardconfiguration.cpp:165
msgid "Please choose a set to be deleted"
msgstr ""

#: keyboardconfiguration.cpp:168
msgid "Do you really want to delete the selected set?"
msgstr ""

#: keyboardconfiguration.cpp:171
msgid "Could not delete set"
msgstr ""

#: keyboardconfiguration.cpp:181 keyboardconfiguration.cpp:203
msgid "Please insert or select a set first"
msgstr ""

#: keyboardconfiguration.cpp:185
msgid "Add keyboard tab"
msgstr ""

#: keyboardconfiguration.cpp:185
msgid "Please enter the name of the new tab:"
msgstr ""

#: keyboardconfiguration.cpp:189
msgid "Failed to add tab"
msgstr ""

#: keyboardconfiguration.cpp:209
msgid "Please select the tab to be edited"
msgstr ""

#: keyboardconfiguration.cpp:213
msgid "Edit keyboard tab"
msgstr ""

#: keyboardconfiguration.cpp:213
msgid "Please enter the new name of the tab:"
msgstr ""

#: keyboardconfiguration.cpp:217
msgid "Failed to edit tab"
msgstr ""

#: keyboardconfiguration.cpp:229
msgid "Please select a set first"
msgstr ""

#: keyboardconfiguration.cpp:234
msgid "Please select the tab to be deleted"
msgstr ""

#: keyboardconfiguration.cpp:238
msgid "Do you really want to delete the selected tab?"
msgstr ""

#: keyboardconfiguration.cpp:241
msgid "Failed to delete tab"
msgstr ""

#: keyboardconfiguration.cpp:251 keyboardconfiguration.cpp:272
#: keyboardconfiguration.cpp:300
msgid "Please select a set to which to add the new button"
msgstr ""

#: keyboardconfiguration.cpp:255 keyboardconfiguration.cpp:276
#: keyboardconfiguration.cpp:304
msgid "Please select a tab to which to add the new button"
msgstr ""

#: keyboardconfiguration.cpp:282 keyboardconfiguration.cpp:310
#: keyboardconfiguration.cpp:371 keyboardconfiguration.cpp:390
msgid "Please select a button to delete from the list"
msgstr ""

#: keyboardconfiguration.cpp:289
msgid "Failed to edit button"
msgstr ""

#: keyboardconfiguration.cpp:314
msgid "Do you really want to delete the selected button?"
msgstr ""

#: keyboardconfiguration.cpp:318
msgid "Failed to delete button"
msgstr ""

#: keyboardconfiguration.cpp:329 keyboardconfiguration.cpp:350
msgid "Please select the tab to be moved"
msgstr ""

#: keyboardconfiguration.cpp:334
msgid "Failed to move tab up. Maybe it is already at the top?"
msgstr ""

#: keyboardconfiguration.cpp:355
msgid "Failed to move tab down. Maybe it is already at the bottom?"
msgstr ""

#: keyboardconfiguration.cpp:377
msgid "Button could not be moved up. Is it already at the top?"
msgstr ""

#: keyboardconfiguration.cpp:396
msgid "Button could not be moved down. Is it already at the bottom?"
msgstr ""

#: keyboardconfiguration.cpp:512
msgid "Failed to load keyboard sets from storage"
msgstr ""

#: keyboardmodifybuttondialog.cpp:35
msgid "Add key"
msgstr ""

#: keyboardmodifybuttondialog.cpp:89 keyboardmodifybuttondialog.cpp:162
msgid "All fields are mandatory"
msgstr ""

#: keyboardmodifybuttondialog.cpp:156
msgid "Invalid button"
msgstr ""

#. i18n: file: modifybuttondlg.ui:51
#. i18n: ectx: property (text), item, widget (KComboBox, cbValueType)
#: keyboardtab.cpp:183 po/rc.cpp:141 rc.cpp:141
msgid "Text"
msgstr ""

#. i18n: file: modifybuttondlg.ui:56
#. i18n: ectx: property (text), item, widget (KComboBox, cbValueType)
#: keyboardtab.cpp:183 po/rc.cpp:144 rc.cpp:144
msgid "Shortcut"
msgstr ""

#: keyboardtab.cpp:201
msgctxt "Name of the key"
msgid "Name"
msgstr ""

#: keyboardtab.cpp:203
msgctxt "Trigger of the key"
msgid "Trigger"
msgstr ""

#: keyboardtab.cpp:205
msgctxt "Type of the key"
msgid "Type"
msgstr ""

#: keyboardtab.cpp:207
msgctxt "Value of the key"
msgid "Value"
msgstr ""

#. i18n: file: keyboardconfigurationdlg.ui:32
#. i18n: ectx: property (text), widget (QCheckBox, cbShowNumpad)
#: po/rc.cpp:3 rc.cpp:3
msgid "Show Numpad"
msgstr ""

#. i18n: file: keyboardconfigurationdlg.ui:39
#. i18n: ectx: property (text), widget (QCheckBox, cbCaseSensitivity)
#: po/rc.cpp:6 rc.cpp:6
msgid "Case sensitive trigger checking"
msgstr ""

#. i18n: file: keyboardconfigurationdlg.ui:60
#. i18n: ectx: property (text), widget (QLabel, label_2)
#: po/rc.cpp:9 rc.cpp:9
msgctxt "Which set (collection of tabs) is currently displayed"
msgid "Set: "
msgstr ""

#. i18n: file: keyboardconfigurationdlg.ui:77
#. i18n: ectx: property (text), widget (QLabel, label_3)
#: po/rc.cpp:12 rc.cpp:12
msgctxt "Which page of the keyboard is now displayed"
msgid "Tab: "
msgstr ""

#. i18n: file: keyboardconfigurationdlg.ui:87
#. i18n: ectx: property (text), widget (KPushButton, pbAddSet)
#. i18n: file: keyboardconfigurationdlg.ui:94
#. i18n: ectx: property (text), widget (KPushButton, pbAddTab)
#. i18n: file: keyboardconfigurationdlg.ui:165
#. i18n: ectx: property (text), widget (KPushButton, pbAddButton)
#. i18n: file: keyboardconfigurationdlg.ui:87
#. i18n: ectx: property (text), widget (KPushButton, pbAddSet)
#. i18n: file: keyboardconfigurationdlg.ui:94
#. i18n: ectx: property (text), widget (KPushButton, pbAddTab)
#. i18n: file: keyboardconfigurationdlg.ui:165
#. i18n: ectx: property (text), widget (KPushButton, pbAddButton)
#: po/rc.cpp:15 po/rc.cpp:18 po/rc.cpp:39 rc.cpp:15 rc.cpp:18 rc.cpp:39
msgid "Add"
msgstr ""

#. i18n: file: keyboardconfigurationdlg.ui:101
#. i18n: ectx: property (text), widget (KPushButton, pbDeleteSet)
#. i18n: file: keyboardconfigurationdlg.ui:108
#. i18n: ectx: property (text), widget (KPushButton, pbDeleteTab)
#. i18n: file: keyboardconfigurationdlg.ui:179
#. i18n: ectx: property (text), widget (KPushButton, pbDeleteButton)
#. i18n: file: keyboardconfigurationdlg.ui:101
#. i18n: ectx: property (text), widget (KPushButton, pbDeleteSet)
#. i18n: file: keyboardconfigurationdlg.ui:108
#. i18n: ectx: property (text), widget (KPushButton, pbDeleteTab)
#. i18n: file: keyboardconfigurationdlg.ui:179
#. i18n: ectx: property (text), widget (KPushButton, pbDeleteButton)
#: po/rc.cpp:21 po/rc.cpp:24 po/rc.cpp:45 rc.cpp:21 rc.cpp:24 rc.cpp:45
msgid "Delete"
msgstr ""

#. i18n: file: keyboardconfigurationdlg.ui:115
#. i18n: ectx: property (text), widget (KPushButton, pbTabUp)
#. i18n: file: keyboardconfigurationdlg.ui:186
#. i18n: ectx: property (text), widget (KPushButton, pbUpButton)
#. i18n: file: keyboardconfigurationdlg.ui:115
#. i18n: ectx: property (text), widget (KPushButton, pbTabUp)
#. i18n: file: keyboardconfigurationdlg.ui:186
#. i18n: ectx: property (text), widget (KPushButton, pbUpButton)
#: po/rc.cpp:27 po/rc.cpp:48 rc.cpp:27 rc.cpp:48
msgctxt "Direction"
msgid "Up"
msgstr ""

#. i18n: file: keyboardconfigurationdlg.ui:122
#. i18n: ectx: property (text), widget (KPushButton, pbEditSet)
#. i18n: file: keyboardconfigurationdlg.ui:129
#. i18n: ectx: property (text), widget (KPushButton, pbEditTab)
#. i18n: file: keyboardconfigurationdlg.ui:172
#. i18n: ectx: property (text), widget (KPushButton, pbEditButton)
#. i18n: file: keyboardconfigurationdlg.ui:122
#. i18n: ectx: property (text), widget (KPushButton, pbEditSet)
#. i18n: file: keyboardconfigurationdlg.ui:129
#. i18n: ectx: property (text), widget (KPushButton, pbEditTab)
#. i18n: file: keyboardconfigurationdlg.ui:172
#. i18n: ectx: property (text), widget (KPushButton, pbEditButton)
#: po/rc.cpp:30 po/rc.cpp:33 po/rc.cpp:42 rc.cpp:30 rc.cpp:33 rc.cpp:42
msgid "Edit"
msgstr ""

#. i18n: file: keyboardconfigurationdlg.ui:136
#. i18n: ectx: property (text), widget (KPushButton, pbTabDown)
#. i18n: file: keyboardconfigurationdlg.ui:193
#. i18n: ectx: property (text), widget (KPushButton, pbDownButton)
#. i18n: file: keyboardconfigurationdlg.ui:136
#. i18n: ectx: property (text), widget (KPushButton, pbTabDown)
#. i18n: file: keyboardconfigurationdlg.ui:193
#. i18n: ectx: property (text), widget (KPushButton, pbDownButton)
#: po/rc.cpp:36 po/rc.cpp:51 rc.cpp:36 rc.cpp:51
msgid "Down"
msgstr ""

#. i18n: file: keyboardwidget.ui:31
#. i18n: ectx: property (title), widget (QGroupBox, gbNumPad)
#: po/rc.cpp:57 rc.cpp:57
msgid "Num pad"
msgstr ""

#. i18n: file: keyboardwidget.ui:52
#. i18n: ectx: property (text), widget (KPushButton, pbNumberBackspace)
#: po/rc.cpp:60 rc.cpp:60
msgid "Number Backspace"
msgstr ""

#. i18n: file: keyboardwidget.ui:61
#. i18n: ectx: property (text), widget (KPushButton, pb7)
#: po/rc.cpp:63 rc.cpp:63
msgid "7"
msgstr ""

#. i18n: file: keyboardwidget.ui:68
#. i18n: ectx: property (text), widget (KPushButton, pb8)
#: po/rc.cpp:66 rc.cpp:66
msgid "8"
msgstr ""

#. i18n: file: keyboardwidget.ui:75
#. i18n: ectx: property (text), widget (KPushButton, pb9)
#: po/rc.cpp:69 rc.cpp:69
msgid "9"
msgstr ""

#. i18n: file: keyboardwidget.ui:82
#. i18n: ectx: property (text), widget (KPushButton, pb4)
#: po/rc.cpp:72 rc.cpp:72
msgid "4"
msgstr ""

#. i18n: file: keyboardwidget.ui:89
#. i18n: ectx: property (text), widget (KPushButton, pb5)
#: po/rc.cpp:75 rc.cpp:75
msgid "5"
msgstr ""

#. i18n: file: keyboardwidget.ui:96
#. i18n: ectx: property (text), widget (KPushButton, pb6)
#: po/rc.cpp:78 rc.cpp:78
msgid "6"
msgstr ""

#. i18n: file: keyboardwidget.ui:103
#. i18n: ectx: property (text), widget (KPushButton, pb1)
#: po/rc.cpp:81 rc.cpp:81
msgid "1"
msgstr ""

#. i18n: file: keyboardwidget.ui:110
#. i18n: ectx: property (text), widget (KPushButton, pb2)
#: po/rc.cpp:84 rc.cpp:84
msgid "2"
msgstr ""

#. i18n: file: keyboardwidget.ui:117
#. i18n: ectx: property (text), widget (KPushButton, pb3)
#: po/rc.cpp:87 rc.cpp:87
msgid "3"
msgstr ""

#. i18n: file: keyboardwidget.ui:124
#. i18n: ectx: property (text), widget (KPushButton, pb0)
#: po/rc.cpp:90 rc.cpp:90
msgid "0"
msgstr ""

#. i18n: file: keyboardwidget.ui:131
#. i18n: ectx: property (text), widget (KPushButton, pbDecimalSeparator)
#: po/rc.cpp:93 rc.cpp:93
msgid "."
msgstr ""

#. i18n: file: keyboardwidget.ui:179
#. i18n: ectx: property (text), widget (KPushButton, pbShift)
#: po/rc.cpp:105 rc.cpp:105
msgid "Shift"
msgstr ""

#. i18n: file: keyboardwidget.ui:189
#. i18n: ectx: property (text), widget (KPushButton, pbCapsLock)
#: po/rc.cpp:108 rc.cpp:108
msgid "Caps lock"
msgstr ""

#. i18n: file: keyboardwidget.ui:228
#. i18n: ectx: property (text), widget (KPushButton, pbOk)
#: po/rc.cpp:114 rc.cpp:114
msgid "Ok"
msgstr ""

#. i18n: file: modifybuttondlg.ui:14
#. i18n: ectx: property (windowTitle), widget (QWidget, ModifyButtonDlg)
#: po/rc.cpp:129 rc.cpp:129
msgid "Add button"
msgstr ""

#. i18n: file: modifybuttondlg.ui:23
#. i18n: ectx: property (text), widget (QLabel, label_4)
#: po/rc.cpp:132 rc.cpp:132
msgctxt "Name of the key"
msgid "Name:"
msgstr ""

#. i18n: file: modifybuttondlg.ui:33
#. i18n: ectx: property (text), widget (QLabel, label_3)
#: po/rc.cpp:135 rc.cpp:135
msgid "Trigger:"
msgstr ""

#. i18n: file: modifybuttondlg.ui:43
#. i18n: ectx: property (text), widget (QLabel, label)
#: po/rc.cpp:138 rc.cpp:138
msgid "Type:"
msgstr ""

#. i18n: file: simonkeyboardpluginui.rc:6
#. i18n: ectx: Menu (commands)
#: po/rc.cpp:147 rc.cpp:147
msgid "Commands"
msgstr ""

#: po/rc.cpp:148 rc.cpp:148
msgctxt "NAME OF TRANSLATORS"
msgid "Your names"
msgstr ""

#: po/rc.cpp:149 rc.cpp:149
msgctxt "EMAIL OF TRANSLATORS"
msgid "Your emails"
msgstr ""
