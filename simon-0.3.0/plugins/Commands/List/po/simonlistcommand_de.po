# Copyright (C) 2010 Peter Grasch
# This file is distributed under the same license as the simon package.
#
# Peter Grasch <grasch@simon-listens.org>, 2009.
msgid ""
msgstr ""
"Project-Id-Version: simonlistcommand_de\n"
"Report-Msgid-Bugs-To: http://sourceforge.net/tracker/?"
"atid=935103&group_id=190872\n"
"POT-Creation-Date: 2010-07-16 20:21+0200\n"
"PO-Revision-Date: 2010-07-16 20:34+0200\n"
"Last-Translator: Peter Grasch <grasch@simon-listens.org>\n"
"Language-Team: American English <kde-i18n-doc@lists.kde.org>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Lokalize 0.3\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"

#: createlistcommandwidget.cpp:136
msgid ""
"Could not find all of the commands that make up this list command.\n"
"\n"
"The missing commands are: %1."
msgstr ""
"Konnte nicht alle Kommandos finden die dieses Listen-Kommando ausmachen.\n"
"\n"
"Die fehlenden Kommandos sind: %1."

#. i18n: file: createlistcommandwidget.ui:20
#. i18n: ectx: property (text), widget (QLabel, label)
#: po/rc.cpp:3 rc.cpp:3
msgid "Command:"
msgstr "Kommando:"

#. i18n: file: createlistcommandwidget.ui:30
#. i18n: ectx: property (text), widget (QPushButton, pbAddCommand)
#: po/rc.cpp:6 rc.cpp:6
msgid "Add Command"
msgstr "Kommando hinzufügen"

#. i18n: file: createlistcommandwidget.ui:52
#. i18n: ectx: property (text), widget (QPushButton, pbRemove)
#: po/rc.cpp:9 rc.cpp:9
msgid "Remove"
msgstr "Löschen"

#. i18n: file: createlistcommandwidget.ui:62
#. i18n: ectx: property (text), widget (QPushButton, pbMoveUp)
#: po/rc.cpp:12 rc.cpp:12
msgid "Move Up"
msgstr "Nach oben"

#. i18n: file: createlistcommandwidget.ui:72
#. i18n: ectx: property (text), widget (QPushButton, pbMoveDown)
#: po/rc.cpp:15 rc.cpp:15
msgid "Move Down"
msgstr "Nach unten"

#: po/rc.cpp:16 rc.cpp:16
msgctxt "NAME OF TRANSLATORS"
msgid "Your names"
msgstr "Peter Grasch"

#: po/rc.cpp:17 rc.cpp:17
msgctxt "EMAIL OF TRANSLATORS"
msgid "Your emails"
msgstr "grasch@simon-listens.org"

#~ msgid "Form"
#~ msgstr "Form"

#~ msgid "Launcher"
#~ msgstr "Starter"

#~ msgid "Plugin"
#~ msgstr "Plug-In"

#~ msgid "Plugin trigger"
#~ msgstr "Plug-In Auslöser"

#~ msgid "Launcher trigger"
#~ msgstr "Starter Auslöser"

#~ msgid "Description"
#~ msgstr "Beschreibung"

#~ msgid "Launcher:"
#~ msgstr "Starter:"

#~ msgid "Add Launcher"
#~ msgstr "Starter hinzufügen"

#~ msgid "Back"
#~ msgstr "Zurück"

#~ msgid "Next"
#~ msgstr "Weiter"

#~ msgid "Zero"
#~ msgstr "Null"

#~ msgid "One"
#~ msgstr "Eins"

#~ msgid "Two"
#~ msgstr "Zwei"

#~ msgid "Three"
#~ msgstr "Drei"

#~ msgid "Four"
#~ msgstr "Vier"

#~ msgid "Five"
#~ msgstr "Fünf"

#~ msgid "Six"
#~ msgstr "Sechs"

#~ msgid "Seven"
#~ msgstr "Sieben"

#~ msgid "Eight"
#~ msgstr "Acht"

#~ msgid "Nine"
#~ msgstr "Neun"

#~ msgid "Cancel"
#~ msgstr "Abbrechen"

#~ msgid "List"
#~ msgstr "Liste"

#~ msgid "Commands"
#~ msgstr "Kommandos"

#~ msgid "[INF] Loading list commands from %1"
#~ msgstr "[INF] Lade Listen-Kommandos von %1"

#~ msgid "[INF] Saving list commands to %1"
#~ msgstr "[INF] Speichere Listen Kommandos nach %1"

#~ msgid "Number"
#~ msgstr "Nummer"

#~ msgid "Command"
#~ msgstr "Kommando"

#~ msgid "Kommando"
#~ msgstr "Kommando"
