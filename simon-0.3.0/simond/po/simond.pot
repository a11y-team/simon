# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"Report-Msgid-Bugs-To: http://sourceforge.net/tracker/?"
"atid=935103&group_id=190872\n"
"POT-Creation-Date: 2010-08-30 15:20+0200\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=CHARSET\n"
"Content-Transfer-Encoding: 8bit\n"

#: config/plugin_exporter.cpp:34
msgid "User"
msgstr ""

#: config/plugin_exporter.cpp:35
msgid "Network"
msgstr ""

#: config/simonduserconfiguration.cpp:73
msgid "Could not connect to database"
msgstr ""

#: config/simonduserconfiguration.cpp:90
msgid "Add a new user"
msgstr ""

#: config/simonduserconfiguration.cpp:100
msgid "Could not add user"
msgstr ""

#: config/simonduserconfiguration.cpp:109
msgid "Do you really want to remove the user from the database?"
msgstr ""

#: config/simonduserconfiguration.cpp:115
msgid "Could not remove User"
msgstr ""

#: config/simonduserconfiguration.cpp:121
msgid "Do you also want to remove the speech model from the server?"
msgstr ""

#: config/simonduserconfiguration.cpp:125
msgid "Could not remove the speech model"
msgstr ""

#: config/simonduserconfiguration.cpp:171
msgid "Change Password for User \"%1\""
msgstr ""

#: config/simonduserconfiguration.cpp:174
msgid "Could not change Password"
msgstr ""

#. i18n: file: config/simondconfiguration.kcfg:11
#. i18n: ectx: label, entry (Port), group (Network)
#. i18n: file: config/simondconfiguration.kcfg:13
#. i18n: ectx: tooltip, entry (Port), group (Network)
#. i18n: file: config/simondconfiguration.kcfg:11
#. i18n: ectx: label, entry (Port), group (Network)
#. i18n: file: config/simondconfiguration.kcfg:13
#. i18n: ectx: tooltip, entry (Port), group (Network)
#: po/rc.cpp:3 po/rc.cpp:6 rc.cpp:3 rc.cpp:6
msgid "The port to use for the server."
msgstr ""

#. i18n: file: config/simondconfiguration.kcfg:16
#. i18n: ectx: label, entry (BindTo), group (Network)
#: po/rc.cpp:9 rc.cpp:9
msgid "whether we want to restrict access to a certain ip."
msgstr ""

#. i18n: file: config/simondconfiguration.kcfg:18
#. i18n: ectx: tooltip, entry (BindTo), group (Network)
#: po/rc.cpp:12 rc.cpp:12
msgid "Whether we want to restrict access to a certain ip."
msgstr ""

#. i18n: file: config/simondconfiguration.kcfg:21
#. i18n: ectx: label, entry (Host), group (Network)
#. i18n: file: config/simondconfiguration.kcfg:23
#. i18n: ectx: tooltip, entry (Host), group (Network)
#. i18n: file: config/simondconfiguration.kcfg:21
#. i18n: ectx: label, entry (Host), group (Network)
#. i18n: file: config/simondconfiguration.kcfg:23
#. i18n: ectx: tooltip, entry (Host), group (Network)
#: po/rc.cpp:15 po/rc.cpp:18 rc.cpp:15 rc.cpp:18
msgid "The only host to allow to connect to the server."
msgstr ""

#. i18n: file: config/simondconfiguration.kcfg:27
#. i18n: ectx: label, entry (UseEncryption), group (Network)
#: po/rc.cpp:21 rc.cpp:21
msgid "whether we want to encrypt the communication between simon and simond."
msgstr ""

#. i18n: file: config/simondconfiguration.kcfg:29
#. i18n: ectx: tooltip, entry (UseEncryption), group (Network)
#: po/rc.cpp:24 rc.cpp:24
msgid "Whether we want to encrypt the communication between simon and simond."
msgstr ""

#. i18n: file: config/simondconfiguration.kcfg:32
#. i18n: ectx: label, entry (EncryptionMethod), group (Network)
#. i18n: file: config/simondconfiguration.kcfg:33
#. i18n: ectx: tooltip, entry (EncryptionMethod), group (Network)
#. i18n: file: config/simondconfiguration.kcfg:32
#. i18n: ectx: label, entry (EncryptionMethod), group (Network)
#. i18n: file: config/simondconfiguration.kcfg:33
#. i18n: ectx: tooltip, entry (EncryptionMethod), group (Network)
#: po/rc.cpp:27 po/rc.cpp:30 rc.cpp:27 rc.cpp:30
msgid "The encryption method to use."
msgstr ""

#. i18n: file: config/simondconfiguration.kcfg:36
#. i18n: ectx: label, entry (Certificate), group (Network)
#: po/rc.cpp:33 rc.cpp:33
msgid "the path to the SSL certificate."
msgstr ""

#. i18n: file: config/simondconfiguration.kcfg:37
#. i18n: ectx: tooltip, entry (Certificate), group (Network)
#: po/rc.cpp:36 rc.cpp:36
msgid "The path to the SSL certificate."
msgstr ""

#. i18n: file: config/simondconfiguration.kcfg:43
#. i18n: ectx: label, entry (KeepRecognitionSamples), group (User)
#: po/rc.cpp:39 rc.cpp:39
msgid "keep temporary samples."
msgstr ""

#. i18n: file: config/simondconfiguration.kcfg:45
#. i18n: ectx: tooltip, entry (KeepRecognitionSamples), group (User)
#: po/rc.cpp:42 rc.cpp:42
msgid "Keep the samples used for the recognition."
msgstr ""

#. i18n: file: config/simondconfiguration.kcfg:50
#. i18n: ectx: label, entry (DatabaseUrl), group (Database)
#. i18n: file: config/simondconfiguration.kcfg:52
#. i18n: ectx: tooltip, entry (DatabaseUrl), group (Database)
#. i18n: file: config/simondconfiguration.kcfg:50
#. i18n: ectx: label, entry (DatabaseUrl), group (Database)
#. i18n: file: config/simondconfiguration.kcfg:52
#. i18n: ectx: tooltip, entry (DatabaseUrl), group (Database)
#: po/rc.cpp:45 po/rc.cpp:48 rc.cpp:45 rc.cpp:48
msgid "The path to the database."
msgstr ""

#. i18n: file: config/simondconfiguration.kcfg:58
#. i18n: ectx: label, entry (ModelBackups), group (ModelManagement)
#: po/rc.cpp:51 rc.cpp:51
msgid "how many model versions we should preserve."
msgstr ""

#. i18n: file: config/simondconfiguration.kcfg:60
#. i18n: ectx: tooltip, entry (ModelBackups), group (ModelManagement)
#: po/rc.cpp:54 rc.cpp:54
msgid ""
"How many model versions we should preserv for the model backup / restore."
msgstr ""

#. i18n: file: config/simondnetworkconfiguration.ui:24
#. i18n: ectx: property (text), widget (QLabel, label_2)
#: po/rc.cpp:57 rc.cpp:57
msgid ""
"<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.0//EN\" \"http://www.w3.org/TR/"
"REC-html40/strict.dtd\">\n"
"<html><head><meta name=\"qrichtext\" content=\"1\" /><style type=\"text/css"
"\">\n"
"p, li { white-space: pre-wrap; }\n"
"</style></head><body style=\" font-family:'DejaVu Sans'; font-size:8pt; font-"
"weight:400; font-style:normal;\">\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-"
"right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-"
"family:'Bitstream Vera Sans'; font-weight:600;\">Note:</span><span style=\" "
"font-family:'Bitstream Vera Sans';\"> You must restart simond for the "
"changes to take effect.</span></p></body></html>"
msgstr ""

#. i18n: file: config/simondnetworkconfiguration.ui:36
#. i18n: ectx: property (text), widget (QLabel, label)
#: po/rc.cpp:64 rc.cpp:64
msgid "Port:"
msgstr ""

#. i18n: file: config/simondnetworkconfiguration.ui:56
#. i18n: ectx: property (text), widget (QCheckBox, kcfg_BindTo)
#: po/rc.cpp:67 rc.cpp:67
msgid "Only allow connections from:"
msgstr ""

#. i18n: file: config/simondnetworkconfiguration.ui:75
#. i18n: ectx: property (title), widget (QGroupBox, kcfg_UseEncryption)
#: po/rc.cpp:70 rc.cpp:70
msgid "Encryption"
msgstr ""

#. i18n: file: config/simondnetworkconfiguration.ui:84
#. i18n: ectx: property (text), widget (QLabel, lbCipher)
#: po/rc.cpp:73 rc.cpp:73
msgid "Algorithm:"
msgstr ""

#. i18n: file: config/simondnetworkconfiguration.ui:94
#. i18n: ectx: property (text), widget (QLabel, lbCert)
#: po/rc.cpp:76 rc.cpp:76
msgid "Certificate:"
msgstr ""

#. i18n: file: config/simondnetworkconfiguration.ui:101
#. i18n: ectx: property (filter), widget (KUrlComboRequester, kcfg_Certificate)
#: po/rc.cpp:79 rc.cpp:79
msgid "SSL-Certificate *.pem"
msgstr ""

#. i18n: file: config/simonduserconfiguration.ui:20
#. i18n: ectx: property (text), widget (QCheckBox, cbKeepSamples)
#: po/rc.cpp:82 rc.cpp:82
msgid "Keep recognition samples"
msgstr ""

#. i18n: file: config/simonduserconfiguration.ui:31
#. i18n: ectx: property (text), widget (QLabel, label_2)
#: po/rc.cpp:85 rc.cpp:85
msgid ""
"<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.0//EN\" \"http://www.w3.org/TR/"
"REC-html40/strict.dtd\">\n"
"<html><head><meta name=\"qrichtext\" content=\"1\" /><style type=\"text/css"
"\">\n"
"p, li { white-space: pre-wrap; }\n"
"</style></head><body style=\" font-family:'DejaVu Sans'; font-size:8pt; font-"
"weight:400; font-style:normal;\">\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-"
"right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-"
"family:'Bitstream Vera Sans'; font-weight:600;\">WARNING:</span><span style="
"\" font-family:'Bitstream Vera Sans';\"> All changes to the user database "
"will be applied instantly.</span></p></body></html>"
msgstr ""

#. i18n: file: config/simonduserconfiguration.ui:43
#. i18n: ectx: property (text), widget (QLabel, label)
#: po/rc.cpp:92 rc.cpp:92
msgid "Database:"
msgstr ""

#. i18n: file: config/simonduserconfiguration.ui:70
#. i18n: ectx: property (text), widget (KPushButton, pbAdd)
#: po/rc.cpp:95 rc.cpp:95
msgid "Add"
msgstr ""

#. i18n: file: config/simonduserconfiguration.ui:77
#. i18n: ectx: property (text), widget (KPushButton, pbChangePassword)
#: po/rc.cpp:98 rc.cpp:98
msgid "Change Password"
msgstr ""

#. i18n: file: config/simonduserconfiguration.ui:84
#. i18n: ectx: property (text), widget (KPushButton, pbDelete)
#: po/rc.cpp:101 rc.cpp:101
msgid "Delete"
msgstr ""

#: po/rc.cpp:102 rc.cpp:102
msgctxt "NAME OF TRANSLATORS"
msgid "Your names"
msgstr ""

#: po/rc.cpp:103 rc.cpp:103
msgctxt "EMAIL OF TRANSLATORS"
msgid "Your emails"
msgstr ""

#: src/juliuscontrol.cpp:417
msgid ""
"The recognition could not be started because your model contains words that "
"consists of sounds that are not covered by your acoustic model.\n"
"\n"
"You need to either remove those words, transcribe them differently or train "
"them.\n"
"\n"
"Warning: The latter will not work if you are using static base models!\n"
"\n"
"This could also be a sign of a base model that uses a different phoneme set "
"than your scenario vocabulary.\n"
"\n"
"The following words are affected (list may not be complete):\n"
"%1\n"
"\n"
"The following phonemes are affected (list may not be complete):\n"
"%2"
msgstr ""

#: src/juliuscontrol.cpp:444
msgid "Could not open log file. Please ignore detailed log output!"
msgstr ""

#: src/juliuscontrol.cpp:452
msgid "Internal Jconf error"
msgstr ""

#: src/juliuscontrol.cpp:464
msgid "Could not initialize recognition"
msgstr ""

#: src/juliuscontrol.cpp:481
msgid "Could not start adin-thread"
msgstr ""

#: src/juliuscontrol.cpp:503 src/juliuscontrol.cpp:530
msgid "Unknown error"
msgstr ""

#: src/juliuscontrol.cpp:506 src/juliuscontrol.cpp:533
msgid "Error with the audio stream"
msgstr ""

#: src/main.cpp:35
msgid ""
"The simon recognition daemon (powered by the Large Vocabulary Continuous "
"Speech Recognition Engine Julius)"
msgstr ""

#: src/main.cpp:39
msgid "simond"
msgstr ""

#: src/main.cpp:40
msgid ""
"(C) 2008-2010 Peter Grasch\n"
"\n"
"Julius:\n"
"Copyright (c) 1997-2000 Information-technology Promotion Agency, Japan\n"
"Copyright (c) 1991-2009 Kawahara Lab., Kyoto University\n"
"Copyright (c) 2000-2005 Shikano Lab., Nara Institute of Science and "
"Technology\n"
"Copyright (c) 2005-2009 Julius project team, Nagoya Institute of Technology\n"
msgstr ""

#: src/main.cpp:46
msgid "Peter Grasch"
msgstr ""
