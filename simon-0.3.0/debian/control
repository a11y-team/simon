Source: simon
Section: utils
Priority: extra
Maintainer: Ubuntu Developers <ubuntu-devel-discuss@lists.ubuntu.com>
XSBC-Original-Maintainer: Peter Grasch <grasch@simon-listens.org>
Build-Depends: debhelper (>= 7.2.3~), cdbs (>= 0.4.51), quilt, cmake, kdelibs5-dev, bison, flex, libxtst-dev, libattica-dev, zlib1g-dev
Standards-Version: 3.8.4
Homepage: http://simon-listens.org

Package: simon
Architecture: any
Depends: ${shlibs:Depends}, ${misc:Depends}, kdelibs5 (>=4.3.98-0ubuntu3), kdebase-runtime, libqtcore4, libqt4-multimedia, libasound2, zlib1g, libqt4-sql-sqlite, libattica0, khelpcenter, sox
Conflicts: julius, julius-simon
Replaces: julius, julius-simon
Description: open source speech recognition
 With simon you can control your computer with your voice. You can 
 open programs, URLs, type configurable text snippets, simulate 
 shortcuts, control the mouse and keyboard and much more.
 simon is not bound to any language and works with any dialect.
 This project utilizes the open source large vocabulary continuous 
 speech recognition engine Julius (this package ships its own 
 modified version).

Package: simon-dev
Section: devel
Architecture: all
Depends: ${misc:Depends}, simon (>= ${source:Version}), simon (<< ${source:Version}.1~)
Description: development files for simon
 This package should be installed if you want to develop applications
 based on the simon libraries or want to extend or change the 
 original simon package.

Package: simon-dbg
Priority: extra
Section: debug
Architecture: any
Depends: simon (= ${binary:Version}), ${misc:Depends}
Recommends: kdelibs5-dbg
Description: debugging symbols for simon
 This package provides debugging symbols for all binary packages built from
 the simon source package. It's highly recommended to have this package 
 installed before reporting any simon crashes to either simon developers 
 or Debian package maintainers.
