<?xml version="1.0" ?>
<!DOCTYPE book PUBLIC "-//KDE//DTD DocBook XML V4.2-Based Variant V1.1//EN" "dtd/kdex.dtd" [
  <!-- Define an entity for your application if it is not part of KDE
       CVS -->
  <!ENTITY kmyapplication "<application
>sscd</application
>">
  <!ENTITY kappname "&kmyapplication;"
><!-- replace kmyapplication here
                                            do *not* replace kappname-->
  <!ENTITY package "kde-module"
><!-- kdebase, kdeadmin, etc.  Leave
                                     this unchanged if your
                                     application is not maintained in KDE CVS -->
  <!ENTITY % addindex "IGNORE">
  <!ENTITY % English "INCLUDE"
> <!-- ONLY If you are writing non-English
                                     original documentation, change
                                     the language here -->

  <!-- Do not define any other entities; instead, use the entities
       from entities/general.entities and $LANG/user.entities. -->
]>
<!-- kdoctemplate v0.9 January 10 2003
     Changes to comments to clarify entity usage January 10 2003
     Minor update to "Credits and Licenses" section on August 24, 2000
     Removed "Revision history" section on 22 January 2001
     Changed to Installation/Help menu entities 18 October 2001
     Other minor cleanup and changes 18 October 2001
     FPI change and minor changes November 2002 -->

<!--
This template was designed by: David Rugge davidrugge@mindspring.com
with lots of help from: Eric Bischoff ebisch@cybercable.tm.fr
and Frederik Fouvry fouvry@sfs.nphil.uni-tuebingen.de
of the KDE DocBook team.

You may freely use this template for writing any sort of KDE documentation.
If you have any changes or improvements, please let us know.

Remember:
- in XML, the case of the <tags
> and attributes is relevant ;
- also, quote all attributes.

Please don't forget to remove all these comments in your final documentation,
thanks ;-).
-->

<!-- ................................................................ -->

<!-- The language must NOT be changed here. -->
<!-- If you are writing original documentation in a language other -->
<!-- than English, change the language above ONLY, not here -->
<book lang="&language;">

<!-- This header contains all of the meta-information for the document such
as Authors, publish date, the abstract, and Keywords -->

<bookinfo>

<othercredit role="translator"
><firstname
>Peter</firstname
><surname
>Grasch</surname
><affiliation
><address
><email
>grasch@simon-listens.org</email
></address
></affiliation
><contrib
>Deutsche Übersetzung</contrib
></othercredit
> 


<!-- Date and version information of the application
Don't forget to include this last date and this last revision number, we
need them for translation coordination !
Please respect the format of the date (YYYY-MM-DD) and of the version
(V.MM.LL), it could be used by automation scripts.
Do NOT change these in the translation. -->

<date
>2010-09-08</date>
<releaseinfo
>0.3</releaseinfo>

<!-- Abstract about this handbook -->

<abstract>
<para
>&kmyapplication; ist die Serverkomponente zum ssc Werkzeug zum sammeln von Aufnahmen. </para>
</abstract>

<keywordset>
<keyword
>KDE</keyword>
<keyword
>kdeutils</keyword>
<keyword
>Kapp</keyword>
<keyword
>simon</keyword>
<keyword
>sample</keyword>
<keyword
>speech</keyword>
<keyword
>Sprache</keyword>
<keyword
>sammeln</keyword>
<keyword
>Aufnahme</keyword>
<keyword
>accessibility</keyword>
</keywordset>

<legalnotice
>&FDLNotice;</legalnotice>

<copyright>
<year
>2009-2010</year>
<holder
>Peter Grasch</holder>
</copyright>

<authorgroup>
<author
><personname
> <firstname
>Peter</firstname
> <othername
>H.</othername
> <surname
>Grasch</surname
> </personname
> <email
>grasch@simon-listens.org</email
> </author>
</authorgroup>


<title
>Das &kmyapplication; Handbuch</title>
</bookinfo>

<chapter id="introduction">
<title
>Einleitung</title>

<para
>&kmyapplication; ist die Serverkomponente zum ssc Werkzeug zum sammeln von Aufnahmen. </para>
<para
>sscd verwaltet Sprecherdaten (Benutzer, Institutionen) sowie die Metadaten der Aufnahmen in einer Datenbank. Außerdem speichert er die Aufnahmen. </para>
<para
>sscd erhält Daten von dem/den ssc Client(s) die über TCP/IP zum Server verbinden. </para>

<para
>Für mehr Information über die Architektur der simon Suite sehen Sie bitte das <ulink url="help:/simon/overview.html#architecture"
>simon Handbuch</ulink
>. Für Informationen über den ssc Client sehen Sie bitte das <ulink url="help:/ssc"
>ssc Handbuch</ulink
></para>
</chapter>

<chapter id="using-kapp">
<title
>&kmyapplication; verwenden</title>

<para
>&kmyapplication; ist eine Kommandozeilenapplikation und besitzt kein Benutzerinterface. Es existieren keinen speziellen Startparameter.</para>


<sect1 id="sscd_directory">
  <title
>Basisordner</title>
  <para
>Der Basisordner von &kmyapplication; beinhaltet die Konfigurationsdatei <filename
>sscd.conf</filename
> und ein Fehlerprotokoll falls Probleme auftreten. Der Ortner beinhaltet auch den <filename
>samples</filename
> Unterordner der die gesamten Sprachdaten beinhaltet.</para>

<para
>Der Pfad zum sscd Ordner hängt von Ihrem Betriebssystem ab: <table frame='all'
><title
>ssc Basisordner</title>
<tgroup cols='2' align='left' colsep='1' rowsep='1'>
<colspec colname='c1'/>
<colspec colname='c2'/>
<thead>
<row>
  <entry
>Microsoft Windows</entry>
  <entry
>GNU/Linux</entry>
</row>
</thead>
<tbody>
<row>
  <entry
><filename
>Installationsordner von sscd.exe (normalerweise: C:\Programme\simon 0.3\bin\sscd.exe</filename
>)</entry>
  <entry
><filename
>/usr/share/sscd</filename
></entry>
</row>
</tbody>
</tgroup>
</table>
</para>
</sect1>

<sect1 id="configuration">
  <title
>Konfiguration</title>

  <para
>Es existiert keine grafische Konfiguration für sscd aber es gibt eine Konfigurationsdatei (<filename
>sscd.conf</filename
>) die im <link linkend="sscd_directory"
>Basisordner</link
> gespeichert ist.</para>

  <para
>Die Standardkonfigurationsdatei ist gut dokumentiert und sollte selbstklärend sein.</para>

  <para
>Bevor Sie sscd starten werden Sie vermutlich zumindest den Eintrag DatabaseUser und DatabasePassword anpassen wollen. Sehen Sie die <link linkend="database"
>Datenbank Sektion</link
> für mehr Informationen.</para>

  <screen
>; This is an example config file and displays the built-in defaults
; sscd will look for this file in:
; Linux:
;    /usr/share/sscd/sscd.conf
; Windows:
;    &lt;sscd installation path&gt;\sscd.conf

[General]
; Change this to use a different database; Because sscd uses db-specific
; commands in places, only QMYSQL is supported at the moment.
; Support for other DBMS can be added extremely easily, tough so please
; feel free to request support through support@simon-listens.org
DatabaseType=QMYSQL

; The host of the DBMS
DatabaseHost=127.0.0.1

; The port of the DBMS; 3306 is the default port of MySQL
DatabasePort=3306

; The database to use; Make sure that you run the supplied create script
; before you use sscd
DatabaseName=ssc

; The username to use when connecting to the DBMS
DatabaseUser=sscuser

; Database password. The default one will obviously not work in most cases
DatabasePassword=CHANGE ME NOW

; Database options. Refer to Qts documentation of QSqlDatabase for details
DatabaseOptions=MYSQL_OPT_RECONNECT=1

; The port the server will listen to; Default: 4440
Port=4440

; Bind the server to a specific client IP; If this is true, the server
; will ignore requests from all but the BoundHost (see below)
Bind=false

; IP of the bound host (if Bind is active)
BindHost=127.0.0.1
  </screen>
</sect1>

<sect1 id="database">
  <title
>Datenbank</title>
  
  <para
>&kmyapplication; speichert die Sprecher und die Aufnahmenendaten (aber nicht die Aufnahmen selbst) ein einer Datenbank. Derzeit werden nur MySQL Datenbanken voll unterstützt. Support für eine neue Datenbank ist aber trivial. Kontaktieren Sie das <ulink url="mailto:support@simon-listens.org"
>simon Team</ulink
> falls notwendig.</para>

  <para
>Um die notwendigen Tabellen einzurichten liefert &kmyapplication; ein entsprechendes Skript <filename
>mysql_create_script.sql</filename
> mit aus das im <link linkend="sscd_directory"
>Basisordner</link
> von &kmyapplication; liegt.</para>


  <para
>Datenbankfehler können in der Fehlerprotokolldatei <filename
>error.log</filename
> gefunden werden die ebenfalls im Basisordner liegt.</para>

</sect1>


<sect1 id="extracting_samples">
  <title
>Gesammelte Aufnahmen extrahieren</title>

  <para
>Um Modelle aus den Aufnahmen die in &kmyapplication; gesammelt wurden zu erstellen müssen Sie diese zuerst aus der Datenbank extrahieren.</para>

  <warning>
    <para
>Weil &kmyapplication; für große Installationen gedacht ist ist dieser Prozess derzeit noch nicht Endanwenderfreundlich. Die Dokumentation hier ist hauptsächlich für technisch versierte Profis gedacht. </para>
    <para
>Die meisten Skripte hier benötigen die GNU Tools (normalerweise standardmäßig installiert auf GNU/Linux). </para>
  </warning>

  <para
>Sie können die folgende Query (kleine Anpassungen sind je nach dem welche Aufnahmen Sie extrahieren wollen eventuell notwendig) verwenden: <screen
>use ssc;
                                                                                                                           
select s.Path, s.Prompt
  from Sample s inner join User u
    on s.UserId = u.UserId inner join UserInInstitution uii
      on u.UserId = uii.UserId inner join SampleType st
        on s.TypeId = st.SampleTypeId inner join Microphone m
          on m.MicrophoneId = s.MicrophoneId
  WHERE st.ExactlyRepeated=1 and uii.InstitutionId = 3
    and (m.MicrophoneId = 1);
    </screen>
  </para>

  <para
>Diese Query wird eine Liste aller Sample von Institution 3 holen die Mikrofon 1 aufgenommen wurden.</para>

  <para
>Sie können diese Liste zum Beispiel verwenden um eine prompts Datei zu erzeugen. <screen
>#!/bin/bash
      sed '1d' $1 
> temp_out
      sed -e 's/\\\\/\//g' -e 's/.*Samples\///g' -e 's/\.wav\t/ /' temp_out 
> $1
      rm temp_out
    </screen>
  </para>

  <para
>Diese Promptsdatei kann dann <ulink url="help:/simon/training.html#import_trainings-data"
>in simon importiert</ulink
> werden. </para>

  <para
>Um das entsprechende Wörterbuch für das Modellkompilieren zu kreieren benötigen Sie eventuell eine Liste aller Sätze die in der prompts-Datei enthalten sind. Sie können so eine Liste mit dem folgenden Skript erstellen: <screen
>#!/bin/bash
      cat $1 | sed -e 's/[0-9\/]* //' | sort | uniq
    </screen>
  </para>
</sect1>
</chapter>


<chapter id="faq">
<title
>Fragen und Antworten</title>

<para
>Um diese Sektion immer aktuell zu halten, wird dieser Teil des Handbuches in unserem <ulink url="http://www.simon-listens.org/wiki/index.php/English:_Troubleshooting"
>online Wiki</ulink
> und somit für alle editierbar verwaltet. </para>

</chapter>

<chapter id="credits">
<title
>Danksagungen und Lizenz</title>

<para
>&kmyapplication; </para>
<para
>Program copyright 2008-2010 Peter Grasch <email
>grasch@simon-listens.org</email
> </para>

<para
>Dokumentation Copyright &copy; 2009-2010 Peter Grasch <email
>grasch@simon-listens.org</email
> </para>

<para
>Peter Grasch<email
>grasch@simon-listens.org</email
></para
> 
&underFDL; &underGPL; </chapter>

<appendix id="installation">
<title
>Installation</title>
<para
>Bitte sehen Sie unser <ulink url="http://www.simon-listens.org/wiki/index.php/English:_Setup"
>online Wiki</ulink
> for eine detaillierte Installationsanleitung.</para>
</appendix>

&documentation.index;
</book>

<!--
Local Variables:
mode: xml
sgml-minimize-attributes:nil
sgml-general-insert-case:lower
sgml-indent-step:0
sgml-indent-data:nil
End:

vim:tabstop=2:shiftwidth=2:expandtab
kate: space-indent on; indent-width 2; tab-width 2; indent-mode none;
-->
